package com.gs.gapp.os.samples.kmltestdata.modelaccess;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.jenerateit.modelaccess.MessageProviderModelAccess;
import org.jenerateit.modelaccess.ModelAccessException;
import org.jenerateit.modelaccess.ModelAccessOptions;

import com.gs.gapp.os.samples.kmltestdata.metamodel.KmlInput;

import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.Placemark;


/**
 * @author mmt
 *
 */
public class KmlModelAccess extends MessageProviderModelAccess {

	@Override
	public void close() {}

	@Override
	public void init(ModelAccessOptions modelAccessOptions) {}

	@Override
	public boolean isOpen() {
		return false;
	}

	@Override
	public boolean isProgram() {
		return false;
	}

	@Override
	public Object open() {
		return null;
	}

	@Override
	public void selectElements(Collection<?> elements) {}

	@Override
	public String getDescription() {
		return "Model access to read GIS data in KML format";
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	protected Collection<?> loadElements(InputStream inputStream,
			ModelAccessOptions modelAccessOptions) throws ModelAccessException {
		
		try {
			List<Object> result = new ArrayList<Object>();
			final ZipInputStream zis = new ZipInputStream(inputStream);
			ZipEntry entry = null;
			while ((entry = zis.getNextEntry()) != null) {
				
				if (entry.getName().toLowerCase().endsWith(".kml")) {
					String simpleName = entry.getName();
					if (simpleName.contains("/")) simpleName = entry.getName().substring(entry.getName().lastIndexOf("/"));
					simpleName = simpleName.replace(".kml", "");
					
					// --- overwriting the close() method in order to do nothing since the given zip input stream will be closed by the callee
					final Kml kml = Kml.unmarshal(new BufferedInputStream(zis) {
						@Override
						public void close() throws IOException {}
					});
					analyzeKmlInput(kml);
					if (kml != null) result.add(new KmlInput(simpleName, kml));
				} else {
					throw new RuntimeException("Found an entry '" + entry.getName() + "' in the zip file stream with an uknown file extension");
				}
				zis.closeEntry();
			}
			return result;
				
		} catch (IOException e) {
			throw new ModelAccessException("Error while reading ZIP input stream content", e);

		} catch (Throwable th) {
			throw new ModelAccessException("Error while creating the KML object from ZIP input stream", th);
		}
	}
	
	/**
	 * @param kml
	 */
	private void analyzeKmlInput(Kml kml) {
		if (kml.getFeature() instanceof de.micromata.opengis.kml.v_2_2_0.Document) {
			de.micromata.opengis.kml.v_2_2_0.Document document = (de.micromata.opengis.kml.v_2_2_0.Document) kml.getFeature();
			for (Feature feature : document.getFeature()) {
				if (feature instanceof Placemark) {
					
				} else {
					System.out.println("document's feature class:" + kml.getFeature().getClass());
				}
			}
		} else {
			System.out.println("feature class:" + kml.getFeature().getClass());
		}
		
	}
}
