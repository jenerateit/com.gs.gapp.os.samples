/**
 * 
 */
package com.gs.gapp.os.samples.kmltestdata.generation;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.os.samples.kmltestdata.metamodel.KmlResult;

/**
 * @author mmt
 *
 */
public class KmlWriter extends AbstractWriter {
	
	@ModelElement
	private KmlResult kmlResult;

	/**
	 * 
	 */
	public KmlWriter() {}

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		try {
			kmlResult.getKml().marshal(os);
			write(os.toByteArray());
		} catch (FileNotFoundException ex) {
			throw new WriterException("not able to write kml to output stream", ex);
		}
	}
}
