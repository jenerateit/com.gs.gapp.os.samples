/*
 * Copyright (c) 2011 Generative Software http://www.generative-software.com
 * All rights reserved.
 * Original author: Heinz Rohmer
 *
 * $Id$
 * $HeadURL$
 * $Author$
 * $Date$
 * $Revision$
 */

package com.gs.gapp.os.samples.kmltestdata.modelaccess;

import org.jenerateit.modelaccess.ModelAccessI;
import org.jenerateit.modelaccess.ModelAccessProviderI;
import org.osgi.service.component.ComponentContext;

/**
 * @author hrr
 *
 */
public class KmlModelAccessProvider implements ModelAccessProviderI {

	private ComponentContext context;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.modelaccess.ModelAccessProviderI#getModelAccessClass()
	 */
	@Override
	public ModelAccessI getModelAccess() {
		return new KmlModelAccess();
	}
	
	public void activate(final ComponentContext context) {
		this.context = context;
	}
	
	public void deactivate() {
		this.context = null;
	}

	public ComponentContext getContext() {
		return context;
	}

	public void setContext(ComponentContext context) {
		this.context = context;
	}
}
