/**
 * 
 */
package com.gs.gapp.os.samples.kmltestdata.metamodel;

import de.micromata.opengis.kml.v_2_2_0.Kml;

/**
 * @author mmt
 *
 */
public class KmlInput {

	private final String name;
	
	private final Kml kml;

	public KmlInput(String name, Kml kml) {
		super();
		this.name = name;
		this.kml = kml;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KmlInput other = (KmlInput) obj;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		return true;
	}

	public Kml getKml() {
		return kml;
	}

	public String getName() {
		return name;
	}
}
