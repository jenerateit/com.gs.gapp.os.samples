package com.gs.gapp.os.samples.kmltestdata.generation;

import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.os.samples.kmltestdata.metamodel.KmlResult;

public class GenerationGroupKml implements GenerationGroupConfigI {

	public GenerationGroupKml() {}

	@Override
	public String getDescription() {
		return this.getName();
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement,
			Class<? extends TargetI<?>> targetClass) {
		if (modelElement instanceof KmlResult) {
			return KmlWriter.class;
		}
		return null;
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement, TargetI<?> targetInstance) {
		return KmlWriter.class;
	}

	@Override
	public Set<Class<? extends TargetI<?>>> getAllTargets() {
		Set<Class<? extends TargetI<?>>> result = new LinkedHashSet<>();
		result.add(KmlTarget.class);
		return result;
	}
}
