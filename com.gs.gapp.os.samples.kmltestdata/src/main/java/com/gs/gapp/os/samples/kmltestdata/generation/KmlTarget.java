/**
 * 
 */
package com.gs.gapp.os.samples.kmltestdata.generation;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTarget;
import org.jenerateit.target.TargetDocumentI;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetI;

import com.gs.gapp.os.samples.kmltestdata.metamodel.KmlResult;

/**
 * @author mmt
 *
 */
public class KmlTarget extends AbstractTarget<KmlTargetDocument> {
	
	
	@ModelElement
	private KmlResult kmlResult;

	@Override
	protected URI getTargetURI() {
		StringBuilder sb = new StringBuilder("testdata/out/").append(kmlResult.getName()).append(".kml");
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, (TargetI<? extends TargetDocumentI>) this);
		}
	}
}
