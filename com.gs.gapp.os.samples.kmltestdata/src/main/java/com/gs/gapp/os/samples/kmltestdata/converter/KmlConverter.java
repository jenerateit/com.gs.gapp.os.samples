/**
 * 
 */
package com.gs.gapp.os.samples.kmltestdata.converter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.modelconverter.MessageProviderModelConverter;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.os.samples.kmltestdata.metamodel.KmlInput;
import com.gs.gapp.os.samples.kmltestdata.metamodel.KmlResult;

import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.ExtendedData;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Geometry;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.Point;
import de.micromata.opengis.kml.v_2_2_0.SchemaData;
import de.micromata.opengis.kml.v_2_2_0.gx.MultiTrack;
import de.micromata.opengis.kml.v_2_2_0.gx.Track;

/**
 * @author mmt
 *
 */
public class KmlConverter extends MessageProviderModelConverter {
	
	public static final double ONE_DEGREE_Y_IN_METERS = 111111.0;

	/**
	 * @param options
	 * @return
	 */
	protected final int getNumberOfCopies(ModelConverterOptions options) {
		int result = 1;
		
		final String key = "kml-testdata-number-of-copies";
		
		if (options != null && options.containsKey(key)) {
		    try {
			    result = new Integer(options.get(key).toString()).intValue();
		    } catch (NumberFormatException ex) {
		    	throw new ModelConverterException("converter option " + key + " is not an integer value", ex);
		    }
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.modelconverter.MessageProviderModelConverter#clientConvert(java.util.Collection, org.jenerateit.modelconverter.ModelConverterOptions)
	 */
	@Override
	protected final Set<Object> clientConvert(Collection<?> elements,
			ModelConverterOptions options) throws ModelConverterException {
		LinkedHashSet<Object> result = new LinkedHashSet<>();
		LinkedHashSet<KmlInput> kmlInputs = new LinkedHashSet<KmlInput>();
		LinkedHashSet<KmlResult> kmlResults = new LinkedHashSet<KmlResult>();
		
		for (Object element : elements) {
			if (element instanceof KmlInput) {
				KmlInput input = (KmlInput) element;
				kmlInputs.add(input);
			}
		}
		
		for (KmlInput kmlInput : kmlInputs) {
			for (int ii=0; ii<getNumberOfCopies(options); ii++) {
				KmlResult kmlResult = createKmlResult(kmlInput, kmlInputs, kmlResults, ii);
				kmlResults.add(kmlResult);
			}
		}
		
		result.addAll(kmlResults);
		return result;
	}
	
	/**
	 * This is the method that is intended to be overwritten by classes that inherit from this class.
	 * In here you can provide custom algorithms to create test data.
	 * 
	 * @param originalRoute
	 * @param kmlInputs
	 * @param kmlResults
	 * @param ii
	 * @return
	 */
	protected KmlResult createKmlResult(KmlInput originalRoute, LinkedHashSet<KmlInput> kmlInputs, LinkedHashSet<KmlResult> kmlResults, final int ii) {
		if (originalRoute == null) throw new NullPointerException("parameter 'originalRoute' must not be null");
		
		// a simple test, should return 250.20613449 according to http://gis.stackexchange.com/questions/29239/calculate-bearing-between-two-decimal-gps-coordinates
//		double d = getDirection(new Coordinate(-70.450696, 43.682213), new Coordinate(-70.450769, 43.682194));
//		System.out.println("direction:" + d);
		
		Kml kml = (Kml) originalRoute.getKml();
		Kml kmlCopy = copyKml(kml);
		
		if (kmlCopy.getFeature() instanceof Document) {
			Document document = (Document) kmlCopy.getFeature();
			for (Feature feature : document.getFeature()) {
				if (feature instanceof Placemark) {
					Placemark placemark = (Placemark) feature;
					Geometry geometry = placemark.getGeometry();
					
					if (geometry instanceof Point) {
						Point point = (Point) geometry;
						// --- move the coordinates
						for (Coordinate coordinate : point.getCoordinates()) {
                            moveCoordinate(coordinate, 0.0, ii*3.0);
						}
					} else if (geometry instanceof MultiTrack) {
						MultiTrack multiTrack = (MultiTrack) geometry;
						int totalNumberOfCoordinates = 0;
						for (Track track : multiTrack.getTrack()) {
							@SuppressWarnings("unused")
							double prevLat = 0.0;
							@SuppressWarnings("unused")
							double prevLng = 0.0;
							double nextLat = 0.0;
							double nextLng = 0.0;
							
							for (int idx=0; idx<track.getCoord().size(); idx++) {
								String[] coords = track.getCoord().get(idx).split(" ");
								double lat = new Double(coords[0]).doubleValue();
								double lng = new Double(coords[1]).doubleValue();
								double alt = new Double(coords[2]).doubleValue();
								totalNumberOfCoordinates++;
								
								
								if (idx < track.getCoord().size()-1) {
									String[] nextCoords = track.getCoord().get(idx+1).split(" ");
									nextLat = new Double(nextCoords[0]).doubleValue();
									nextLng = new Double(nextCoords[1]).doubleValue();
									
									@SuppressWarnings("unused")
									double direction = getDirection(new Coordinate(nextLng, nextLat), new Coordinate(lng, lat));
								}
								
								String when = track.getWhen().get(idx);
								
								analyzeExtendedData(track.getExtendedData());

								
								Coordinate coordinate = new Coordinate(lng, lat, alt);
								moveCoordinate(coordinate, 0.0, ii*3.0);
								track.getCoord().set(idx, coordinate.getLatitude() + " " + coordinate.getLongitude() + " " + coordinate.getAltitude());
								track.getWhen().set(idx, when);
								
								
								prevLat = lat;
								prevLng = lng;
							}
							
						} // for all tracks in multi track
						
						System.out.println("total # of tracks in multi track:" + multiTrack.getTrack().size());
						System.out.println("total # of coordinates:" + totalNumberOfCoordinates);
					}
				} else {
					System.out.println("feature's class:" + feature.getClass());
				}
			}
		}
		
		KmlResult kmlResult = new KmlResult(originalRoute.getName() + "-" + ii + ".kml", kmlCopy);
		return kmlResult;
	}
	
	/**
	 * @param extendedData
	 */
	private void analyzeExtendedData(ExtendedData extendedData) {
		if (extendedData == null) return;
		
		System.out.println("extended data getAny():" + extendedData.getAny());
		System.out.println("extended data getData():" + extendedData.getData());
		for (SchemaData schemaData : extendedData.getSchemaData()) {
			System.out.println("schema data target id:" + schemaData.getTargetId());
			
			System.out.println("sd size:" + schemaData.getSimpleData().size());
			System.out.println("sde:" + schemaData.getSchemaDataExtension());
			System.out.println("sd" + schemaData.getSimpleData());
		}
	}
	
	/**
	 * @param start
	 * @param end
	 * @return
	 */
	protected double getDirection(Coordinate start, Coordinate end) {
		double startLat = Math.toRadians(start.getLatitude());
		double startLong = Math.toRadians(start.getLongitude());
		double endLat = Math.toRadians(end.getLatitude());
		double endLong = Math.toRadians(end.getLongitude());

		double dLong = endLong - startLong;

		double dPhi = Math.log(Math.tan(endLat/2.0+Math.PI/4.0)/Math.tan(startLat/2.0+Math.PI/4.0));
		if (Math.abs(dLong) > Math.PI) {
		     if (dLong > 0.0) {
		         dLong = -(2.0 * Math.PI - dLong);
		     } else {
		         dLong = (2.0 * Math.PI + dLong);
		     }
		}

		double bearing = (Math.toDegrees(Math.atan2(dLong, dPhi)) + 360.0) % 360.0;

		return bearing;
	}
	
	/**
	 * @param coordinate
	 * @param meterX
	 * @param meterY
	 */
	protected final void moveCoordinate(Coordinate coordinate, double meterX, double meterY) {
		double lat = coordinate.getLatitude();
		double lng = coordinate.getLongitude();
		
		double offsetLatitude = (meterY / ONE_DEGREE_Y_IN_METERS);
		double offsetLongitude = (meterX / (ONE_DEGREE_Y_IN_METERS * Math.cos(lat)));
		
		coordinate.setLatitude(lat + offsetLatitude);
		coordinate.setLongitude(lng + offsetLongitude);
	}
	
	/**
	 * @param kml
	 * @return
	 */
	protected final Kml copyKml(Kml kml) {
		if (kml == null) throw new NullPointerException("parameter 'kml' must not be null");
		
		Kml result = null;
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		try {
			kml.marshal(baos);
			byte[] kmlAsByteArray = baos.toByteArray();
			result = Kml.unmarshal(new ByteArrayInputStream(kmlAsByteArray));
		} catch (FileNotFoundException ex) {
			throw new ModelConverterException("unable to marshal existing kml object", ex);
		}
		
		return result;
	}
}
