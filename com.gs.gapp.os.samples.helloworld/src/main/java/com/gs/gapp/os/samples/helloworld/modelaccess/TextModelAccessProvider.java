package com.gs.gapp.os.samples.helloworld.modelaccess;

import org.jenerateit.modelaccess.ModelAccessI;
import org.jenerateit.modelaccess.ModelAccessProviderI;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Component;

/**
 *
 */
@Component(name="TextModelAccessProvider")
public class TextModelAccessProvider implements ModelAccessProviderI {

	private ComponentContext context;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.modelaccess.ModelAccessProviderI#getModelAccessClass()
	 */
	@Override
	public ModelAccessI getModelAccess() {
		return new TextModelAccess();
	}
	
	public void activate(final ComponentContext context) {
		this.context = context;
	}
	
	public void deactivate() {
		this.context = null;
	}

	public ComponentContext getContext() {
		return context;
	}

	public void setContext(ComponentContext context) {
		this.context = context;
	}
}
