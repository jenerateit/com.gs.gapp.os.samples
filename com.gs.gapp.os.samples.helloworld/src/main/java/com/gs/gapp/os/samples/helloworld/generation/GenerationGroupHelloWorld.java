package com.gs.gapp.os.samples.helloworld.generation;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.target.AbstractTextTarget;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetI;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.os.samples.helloworld.generation.GenerationGroupHelloWorld.HelloWorldTarget.HelloWorldWriter;
import com.gs.gapp.os.samples.helloworld.metamodel.Person;

/**
 *
 */
public class GenerationGroupHelloWorld implements GenerationGroupConfigI {

	@Override
	public String getDescription() {
		return this.getName();
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.WriterLocatorI#getWriterClass(java.lang.Object, java.lang.Class)
	 */
	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement,
			Class<? extends TargetI<?>> targetClass) {
		
        if (modelElement instanceof Person && targetClass == HelloWorldTarget.class) {
            return HelloWorldWriter.class;
        }
		
		return null;
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.WriterLocatorI#getWriterClass(java.lang.Object, org.jenerateit.target.TargetI)
	 */
	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement,
			TargetI<?> targetInstance) {
		
		@SuppressWarnings({ "unchecked" })
		Class<? extends TargetI<?>> targetClazz =
		    (Class<? extends TargetI<?>>) targetInstance.getClass();
		
		Class<? extends WriterI> writerClass =
	        getWriterClass(modelElement, targetClazz);
		
		return writerClass;
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupConfigI#getAllTargets()
	 */
	@Override
	public Set<Class<? extends TargetI<?>>> getAllTargets() {
		
		Set<Class<? extends TargetI<?>>> result = new LinkedHashSet<>();
        result.add(HelloWorldTarget.class);
		return result;
	}
	
	/**
	 *
	 */
	public static class HelloWorldTarget extends AbstractTextTarget<HelloWorldTargetDocument> {
		
		
		@ModelElement
		private Person modelElement;
		
		/* (non-Javadoc)
		 * @see org.jenerateit.target.AbstractTarget#getTargetURI()
		 */
		@Override
		protected URI getTargetURI() {
		    // TODO add your own path rules (directories, file ending, ...)
			StringBuilder sb = new StringBuilder("./").append(modelElement.getName()).append(".txt");
			
			try {
			    return new URI(sb.toString());
			} catch (URISyntaxException e) {
				throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
			}
		}
		
		public static class HelloWorldWriter extends AbstractTextWriter {
			
			@ModelElement
			private Person modelElement;
			
			@Override
		    public void transform(TargetSection ts) throws WriterException {
				wNL("Hello ", modelElement.getName(), "!");
			}
		}
	}
}
