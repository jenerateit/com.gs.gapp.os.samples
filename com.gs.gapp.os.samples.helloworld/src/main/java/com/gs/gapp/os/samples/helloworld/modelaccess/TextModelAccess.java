package com.gs.gapp.os.samples.helloworld.modelaccess;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.jenerateit.modelaccess.MessageProviderModelAccess;
import org.jenerateit.modelaccess.ModelAccessException;
import org.jenerateit.modelaccess.ModelAccessOptions;


/**
 *
 */
public class TextModelAccess extends MessageProviderModelAccess {

	@Override
	public void close() {}

	@Override
	public void init(ModelAccessOptions modelAccessOptions) {}

	@Override
	public boolean isOpen() {
		return false;
	}

	@Override
	public boolean isProgram() {
		return false;
	}

	@Override
	public Object open() {
		return null;
	}

	@Override
	public void selectElements(Collection<?> elements) {}

	@Override
	public String getDescription() {
		return "TODO add model access description here";
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	protected Collection<?> loadElements(InputStream inputStream, ModelAccessOptions modelAccessOptions) throws ModelAccessException {
		
		try {
			List<Object> result = new ArrayList<>();
			final ZipInputStream zis = new ZipInputStream(inputStream);
			ZipEntry entry = null;
			while ((entry = zis.getNextEntry()) != null) {
				if (entry.getName().toLowerCase().endsWith(".txt")) {
					
					
					// --- overwriting the close() method in order to do nothing since the given zip input stream will be closed by the caller
					BufferedInputStream bis = new BufferedInputStream(zis) {
						@Override
						public void close() throws IOException {}
					};
					
					String input = getStringFromInputStream(bis);
					String[] names = input.split("[\n]");
					
					for (String name : names) {
						if (name != null && name.trim().length() > 0) {
							result.add(name.trim());
						}
					}
				} else {
					// ignore this entry since it doesn't represent a file that this model access is supposed to handle
				}
				zis.closeEntry();
			}
			return result;
				
		} catch (IOException ex) {
			throw new ModelAccessException("Error while reading ZIP input stream content", ex);

		} catch (Throwable th) {
			throw new ModelAccessException("Error while creating model elements from ZIP input stream", th);
		}
	}
	
	private String getStringFromInputStream(InputStream is) {
 
		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();
 
		String line;
		try {
 			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
				sb.append(System.getProperty("line.separator"));
			}
 		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
 
		return sb.toString();
 	}
}
