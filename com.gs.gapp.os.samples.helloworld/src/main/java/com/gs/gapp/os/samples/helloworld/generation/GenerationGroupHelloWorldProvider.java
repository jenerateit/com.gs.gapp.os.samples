package com.gs.gapp.os.samples.helloworld.generation;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupProviderI;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Component;

/**
 *
 */
@Component(name="GenerationGroupHelloWorldProvider")
public class GenerationGroupHelloWorldProvider implements GenerationGroupProviderI {

	private ComponentContext context;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupProviderI#getGenerationGroupConfig()
	 */
	@Override
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupHelloWorld();
	}
	
	public void activate(final ComponentContext context) {
		this.context = context;
	}
	
	public void deactivate() {
		this.context = null;
	}

	public ComponentContext getContext() {
		return context;
	}

	public void setContext(ComponentContext context) {
		this.context = context;
	}
}
