package com.gs.gapp.os.samples.helloworld.metamodel;

public class Person {

    private final String name;
    
    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}