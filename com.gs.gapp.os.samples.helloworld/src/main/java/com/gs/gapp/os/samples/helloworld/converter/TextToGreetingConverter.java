package com.gs.gapp.os.samples.helloworld.converter;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.modelconverter.MessageProviderModelConverter;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.os.samples.helloworld.metamodel.Person;

/**
 *
 */
public class TextToGreetingConverter extends MessageProviderModelConverter {
	
	@Override
	protected final Set<Object> clientConvert(Collection<?> elements,
			ModelConverterOptions options) throws ModelConverterException {
		
		LinkedHashSet<Object> result = new LinkedHashSet<>();
		
		for (Object element : elements) {
			if (element instanceof String) {
				String name = (String) element;
                Person person = new Person(name);
                result.add(person);
			}
		}
		
		return result;
	}
}
