/**
 * 
 */
package com.gs.gapp.samples.xjc.generation;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.samples.xjc.metamodel.JavaSourceFile;

/**
 * @author mmt
 *
 */
public class JavaSourceFileWriter extends AbstractWriter {
	
	@ModelElement
	private JavaSourceFile javaSourceFile;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
		write(javaSourceFile.getOutputStream().toByteArray());
	}
}
