package com.gs.gapp.samples.xjc.generation;

import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.samples.xjc.metamodel.JavaSourceFile;

/**
 * @author mmt
 *
 */
public class GenerationGroupJavaSourceFile implements GenerationGroupConfigI {

	@Override
	public String getDescription() {
		return this.getName();
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.WriterLocatorI#getWriterClass(java.lang.Object, java.lang.Class)
	 */
	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement,
			Class<? extends TargetI<?>> targetClass) {
		if (modelElement instanceof JavaSourceFile) {
			return JavaSourceFileWriter.class;
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.WriterLocatorI#getWriterClass(java.lang.Object, org.jenerateit.target.TargetI)
	 */
	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement, TargetI<?> targetInstance) {
		return JavaSourceFileWriter.class;
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupConfigI#getAllTargets()
	 */
	@Override
	public Set<Class<? extends TargetI<?>>> getAllTargets() {
		Set<Class<? extends TargetI<?>>> result = new LinkedHashSet<>();
		result.add(JavaSourceFileTarget.class);
		return result;
	}
}
