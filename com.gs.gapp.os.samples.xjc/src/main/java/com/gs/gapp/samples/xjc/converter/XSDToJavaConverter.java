/**
 * 
 */
package com.gs.gapp.samples.xjc.converter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.jenerateit.modelconverter.MessageProviderModelConverter;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;
import org.xml.sax.SAXParseException;

import com.gs.gapp.samples.xjc.metamodel.JavaSourceFile;
import com.gs.gapp.samples.xjc.metamodel.XSDInput;
import com.sun.codemodel.internal.CodeWriter;
import com.sun.codemodel.internal.JCodeModel;
import com.sun.codemodel.internal.JPackage;
import com.sun.tools.internal.xjc.api.ErrorListener;
import com.sun.tools.internal.xjc.api.S2JJAXBModel;
import com.sun.tools.internal.xjc.api.SchemaCompiler;
import com.sun.tools.internal.xjc.api.XJC;

/**
 * @author mmt
 *
 */
public class XSDToJavaConverter extends MessageProviderModelConverter {
	

	/* (non-Javadoc)
	 * @see org.jenerateit.modelconverter.MessageProviderModelConverter#clientConvert(java.util.Collection, org.jenerateit.modelconverter.ModelConverterOptions)
	 */
	@Override
	protected final Set<Object> clientConvert(Collection<?> elements, ModelConverterOptions options) throws ModelConverterException {
		LinkedHashSet<Object> result = new LinkedHashSet<>();
		
		String packageName = getPackageName(options);
		SchemaCompiler schemaCompiler = XJC.createSchemaCompiler();
		schemaCompiler.setDefaultPackageName(packageName);
		schemaCompiler.setErrorListener(new ErrorListener() {
			
			@Override
			public void warning(SAXParseException arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void info(SAXParseException arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void fatalError(SAXParseException arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void error(SAXParseException arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		for (Object element : elements) {
			if (element instanceof XSDInput) {
				XSDInput xsdInput = (XSDInput) element;
				XMLStreamReader xmlReader;
				
				try {
					xmlReader = XMLInputFactory.newInstance().createXMLStreamReader(new java.io.ByteArrayInputStream(xsdInput.getContent()));
					schemaCompiler.parseSchema(packageName, xmlReader);
				} catch (XMLStreamException | FactoryConfigurationError ex) {
					throw new ModelConverterException("problems while processing XSD file", ex);
				}
			}
		}
		
		S2JJAXBModel model = schemaCompiler.bind();
		JCodeModel jCodeModel = model.generateCode(null, null);
		MyCodeWriter codeWriter = new MyCodeWriter();
		
		try {
			jCodeModel.build(codeWriter);
		} catch (IOException ex) {
            throw new ModelConverterException("failed to build Java source files", ex);
		}
		
		
		for (JavaSourceFile javaSourceFile : codeWriter.getSourceFiles()) {
			result.add(javaSourceFile);
		}
		
		return result;
	}
	
	/**
	 * @param options
	 * @return
	 */
	private String getPackageName(ModelConverterOptions options) {
		String packageName = "com.gs.vd";
		
		if (options != null) {
			packageName = (String) options.get("default-package-name");
		}
		
		return packageName;
	}

	/**
	 * @author mmt
	 *
	 */
	private static class MyCodeWriter extends CodeWriter {
		
		private final LinkedHashSet<JavaSourceFile> sourceFiles = new LinkedHashSet<>();
		

		@Override
		public void close() throws IOException {
			// intentionally leaving this empty - we are not writing to a file
		}

		@Override
		public OutputStream openBinary(JPackage pkg, String fileName)
				throws IOException {
			
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			getSourceFiles().add( new JavaSourceFile(pkg, fileName, outputStream) );
			return outputStream;
		}

		public LinkedHashSet<JavaSourceFile> getSourceFiles() {
			return sourceFiles;
		}
	}
}
