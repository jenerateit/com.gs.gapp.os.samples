/**
 * 
 */
package com.gs.gapp.samples.xjc.metamodel;

import java.io.ByteArrayOutputStream;

import com.sun.codemodel.internal.JPackage;


/**
 * @author mmt
 *
 */
public class JavaSourceFile {

	private final JPackage pkg;
	private final String fileName;
	private final ByteArrayOutputStream outputStream;
	
	/**
	 * @param pkg
	 * @param fileName
	 * @param outputStream
	 */
	public JavaSourceFile(JPackage pkg, String fileName, ByteArrayOutputStream outputStream) {
		super();
		this.pkg = pkg;
		this.fileName = fileName;
		this.outputStream = outputStream;
	}

	public JPackage getPkg() {
		return pkg;
	}

	public String getFileName() {
		return fileName;
	}

	public ByteArrayOutputStream getOutputStream() {
		return outputStream;
	}
	
	/**
	 * @return
	 */
	public String getPackagePath() {
		return this.pkg.name().replace(".", "/");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + ((pkg == null) ? 0 : pkg.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		JavaSourceFile other = (JavaSourceFile) obj;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		if (pkg == null) {
			if (other.pkg != null)
				return false;
		} else if (!pkg.equals(other.pkg))
			return false;
		return true;
	}
	
	
}
