/**
 * 
 */
package com.gs.gapp.samples.xjc.metamodel;


/**
 * @author mmt
 *
 */
public class XSDInput {

	private final String name;
	
	private final byte[] content;
	
	/**
	 * @param name
	 * @param content
	 */
	public XSDInput(String name, byte[] content) {
		this.name = name;
		this.content = content;
	}
	
	public String getName() {
		return name;
	}

	public byte[] getContent() {
		return content;
	}
}
