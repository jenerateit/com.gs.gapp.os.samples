package com.gs.gapp.samples.xjc.modelaccess;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.jenerateit.modelaccess.MessageProviderModelAccess;
import org.jenerateit.modelaccess.ModelAccessException;
import org.jenerateit.modelaccess.ModelAccessOptions;

import com.gs.gapp.samples.xjc.metamodel.XSDInput;


/**
 * @author mmt
 *
 */
public class XSDModelAccess extends MessageProviderModelAccess {

	@Override
	public void close() {}

	@Override
	public void init(ModelAccessOptions modelAccessOptions) {}

	@Override
	public boolean isOpen() {
		return false;
	}

	@Override
	public boolean isProgram() {
		return false;
	}

	@Override
	public Object open() {
		return null;
	}

	@Override
	public void selectElements(Collection<?> elements) {}

	@Override
	public String getDescription() {
		return "Model access to read an XSD file and convert it to a byte array (inside an instance of XSDInput)";
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.modelaccess.MessageProviderModelAccess#loadElements(java.io.InputStream, org.jenerateit.modelaccess.ModelAccessOptions)
	 */
	@Override
	protected Collection<?> loadElements(InputStream inputStream,
			ModelAccessOptions modelAccessOptions) throws ModelAccessException {
		
		try {
			List<Object> result = new ArrayList<Object>();
			final ZipInputStream zis = new ZipInputStream(inputStream);
			ZipEntry entry = null;
			while ((entry = zis.getNextEntry()) != null) {
				
				if (entry.getName().toLowerCase().endsWith(".xsd")) {
					
					String simpleName = entry.getName();
					if (simpleName.contains("/")) simpleName = entry.getName().substring(entry.getName().lastIndexOf("/")+1);
					simpleName = simpleName.replace(".xsd", "");
					
					// --- overwriting the close() method in order to do nothing since the given zip input stream will be closed by the callee
					BufferedInputStream bufferedInputStream = new BufferedInputStream(zis) {
						@Override
						public void close() throws IOException {}
					};
					
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					byte[] buf=new byte[1024];
			        int n;
			        while ((n=bufferedInputStream.read(buf, 0, 1024)) > -1) out.write(buf, 0, n);
			        
					
					XSDInput xsdInput = new XSDInput(simpleName, out.toByteArray());
					result.add(xsdInput);
				} else {
					throw new RuntimeException("Found an entry '" + entry.getName() + "' in the zip file stream with an uknown file extension");
				}
				zis.closeEntry();
			}
			return result;
				
		} catch (IOException e) {
			throw new ModelAccessException("Error while reading ZIP input stream content", e);

		} catch (Throwable th) {
			throw new ModelAccessException("Error while creating the Properties object from ZIP input stream", th);
		}
	}
}
