/**
 * 
 */
package com.gs.gapp.samples.xjc.generation;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTarget;
import org.jenerateit.target.TargetException;

import com.gs.gapp.samples.xjc.metamodel.JavaSourceFile;

/**
 * @author mmt
 *
 */
public class JavaSourceFileTarget extends AbstractTarget<JavaSourceFileTargetDocument> {
	
	
	@ModelElement
	private JavaSourceFile javaSourceFile;
	
	@Override
	protected URI getTargetURI() {
		StringBuilder sb = new StringBuilder(getSourcePath()).append("/").append(getNamespace()).append("/").append(javaSourceFile.getFileName());
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
		}
	}
	
	/**
	 * @return
	 */
	private String getNamespace() {
		return javaSourceFile.getPackagePath();
	}

	/**
	 * @return
	 */
	private String getSourcePath() {
		String sourcePath = (String) getGenerationGroup().getOption("source-path");
		return sourcePath == null || sourcePath.length() == 0 ? "." : sourcePath;
	}
}
