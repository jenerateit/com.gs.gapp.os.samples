# Samples for Virtual Developer

[![Eclipse License](https://img.shields.io/badge/license-Eclipse-brightgreen.svg)](https://www.eclipse.org/legal/epl-v10.html)


[![Virtual Developer](https://www.virtual-developer.com/wp-content/uploads/2018/01/VD-logo.png)](https://www.virtual-developer.com/)


### Overview

In this public repository you find some sample code generators that were developed with the Virtual Developer IDE and that run on the Virtual Developer Platform.

### Getting Started with Virtual Developer

Check out the Virtual Developer [Homepage](https://www.virtual-developer.com/) and [Wiki](https://docs.virtual-developer.com/technical-documentation/) for information. Don't hesitate to [contact](https://www.virtual-developer.com/kontakt/) us.


### License

Published under the [Eclipse Public License 1.0](https://www.eclipse.org/legal/epl-v10.html) 

### Installation

By doing the following steps, you can run the generators within your Eclipse IDE:

- Right-click on the file com.gs.gapp.os.samples.target.tpd in the project com.gs.gapp.os.samples.target and choose the menu option "Set as Target Platform".
- Once the target platform is successfully set, all generators should compile successfully. If not, then restart Eclipse and see whether this makes any difference.
- When opening the run configuration dialog, you should find 7 preconfigured so-called generation units. Run "All Samples".
- By using Window -> Show View- Other ... open the Projektkonfiguration view in the Virtual Developer category.
- Therein create a new generation project by clicking on the green cross icon.
- Add the generator com.gs.gapp.os.samples.helloworld to the generation project.
- Drag the file generation/input/names.txt from the Eclipse project com.gs.gapp.os.samples.helloworld and drop it on your generation project.
- Drag the Eclipse project "Generation Output" and drop it on the <not selected> cell in the "Workspace Projekte" column.
- Right click on the generation project and select "Start" (this runs the code generation).
- You should now find generated files in the Eclipse project "Generation Output".

### Provided by

[![Generative Software GmbH](https://www.virtual-developer.com/wp-content/uploads/2011/06/2011-05-17-IMG-GenerativeSoft-Logo-480x134-e1307098095106-300x83.png)](https://www.generative-software.com/)


# README #

