package com.gs.gapp.os.samples.gengen.metamodel;

public class MetaModelType {

	private final String className;
	private final Genset genset;

	/**
	 * @param className
	 * @param genset
	 */
	public MetaModelType(String className, Genset genset) {
		super();
		this.className = className;
		this.genset = genset;
		
		genset.addMetaModelType(this);
	}

	public String getClassName() {
		return className;
	}

	public Genset getGenset() {
		return genset;
	}
	
	public String getQualifiedPackageName() {
		return new StringBuilder(this.genset.getBasePackageName()).append(".metamodel").toString();
	}
	
	public String getQualifiedClassName() {
		return new StringBuilder(getQualifiedPackageName()).append(".").append(className).toString();
	}
}
