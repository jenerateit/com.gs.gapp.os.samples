/**
 * 
 */
package com.gs.gapp.os.samples.gengen.generation.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTextTarget;
import org.jenerateit.target.TargetException;

import com.gs.gapp.os.samples.gengen.generation.AbstractGenGenWriter;
import com.gs.gapp.os.samples.gengen.generation.JavaTargetDocument;
import com.gs.gapp.os.samples.gengen.metamodel.TransformationStep;

/**
 * @author mmt
 *
 */
//@OneOff
public class ProviderClassTarget extends AbstractTextTarget<JavaTargetDocument> {
	
	@ModelElement
	private TransformationStep transformationStep;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.target.AbstractTarget#getTargetURI()
	 */
	@Override
	protected URI getTargetURI() {
		StringBuilder sb = new StringBuilder("src/main/java/").append(getNamespace()).append("/").append(transformationStep.getProviderClassName()).append(".java");
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
		}
	}
	
	/**
	 * @return
	 */
	private String getNamespace() {
		return transformationStep.getPathSegmentForPackage();
	}
	
	
	public static class ProviderClassWriter extends AbstractGenGenWriter {
		
		@ModelElement
		private TransformationStep transformationStep;
		
		@Override
		protected String getTemplateName() {
			String templateName = null;
			
			switch (transformationStep.getTransformationStepType()) {
			case GENERATION_GROUP:
				templateName = "ProviderClassGenerationGroup.vm";
				break;
			case MODEL_ACCESS:
				templateName = "ProviderClassModelAccess.vm";
				break;
			case MODEL_CONVERTER:
				templateName = "ProviderClassModelConverter.vm";
				break;
			default:
				break;
			}
			return templateName;
		}
		
		@Override
		protected Object getModelElement() {
			return transformationStep;
		}
	}
}
