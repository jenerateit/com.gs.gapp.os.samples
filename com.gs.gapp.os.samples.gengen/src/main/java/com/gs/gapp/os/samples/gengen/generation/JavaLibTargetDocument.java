/**
 * 
 */
package com.gs.gapp.os.samples.gengen.generation;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jenerateit.target.AbstractTargetDocument;
import org.jenerateit.target.TargetSection;

/**
 * @author mmt
 *
 */
public class JavaLibTargetDocument extends AbstractTargetDocument {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2369637221123399820L;

	public static final TargetSection LIB = new TargetSection("lib", 10);

	private static final SortedSet<TargetSection> SECTIONS = new TreeSet<TargetSection>(
			Arrays.asList(new TargetSection[] {
					LIB
			}));

	/**
	 * 
	 */
	public JavaLibTargetDocument() {}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetDocumentI#getTargetSections()
	 */
	@Override
	public SortedSet<TargetSection> getTargetSections() {
		return SECTIONS;
	}

}
