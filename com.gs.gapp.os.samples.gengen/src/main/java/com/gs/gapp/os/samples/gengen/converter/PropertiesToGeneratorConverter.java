/**
 * 
 */
package com.gs.gapp.os.samples.gengen.converter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;

import org.jenerateit.modelconverter.MessageProviderModelConverter;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.os.samples.gengen.metamodel.DeclarativeService;
import com.gs.gapp.os.samples.gengen.metamodel.Genset;
import com.gs.gapp.os.samples.gengen.metamodel.MetaModelType;
import com.gs.gapp.os.samples.gengen.metamodel.TransformationStep;
import com.gs.gapp.os.samples.gengen.metamodel.TransformationStep.TargetDefinition;
import com.gs.gapp.os.samples.gengen.metamodel.TransformationStepType;

/**
 * @author mmt
 *
 */
public class PropertiesToGeneratorConverter extends MessageProviderModelConverter {
	

	/* (non-Javadoc)
	 * @see org.jenerateit.modelconverter.MessageProviderModelConverter#clientConvert(java.util.Collection, org.jenerateit.modelconverter.ModelConverterOptions)
	 */
	@Override
	protected final Set<Object> clientConvert(Collection<?> elements,
			ModelConverterOptions options) throws ModelConverterException {
		
		LinkedHashSet<Object> result = new LinkedHashSet<>();
		
		for (Object element : elements) {
			if (element instanceof Properties) {
				if (result.size() > 0) throw new ModelConverterException("More than one Properties file found in incoming list of model elements");
				Properties properties = (Properties) element;
				
				String generatorName = properties.getProperty(Key.GENERATOR.getKey());
				String basePackageName = properties.getProperty(Key.BASE_PACKAGE.getKey());
				Genset genset = new Genset(generatorName, basePackageName);
				DeclarativeService declarativeService = null;
				
				// --- model access step
				final String modelAccessName = properties.getProperty(Key.ACCESS.getKey());
				final String modelAccessProviderClassName = modelAccessName != null && modelAccessName.length() > 0 ? modelAccessName+"ModelAccessProvider" : "";
				final String modelAccessClassName = modelAccessName != null && modelAccessName.length() > 0 ? modelAccessName+"ModelAccess" : "";
				
				if (properties.containsKey(Key.ACCESS.getKey())) declarativeService = new DeclarativeService(TransformationStepType.MODEL_ACCESS.getPackagePostfix()+".xml", "org.jenerateit.modelaccess.ModelAccessProviderI");
				TransformationStep modelAccess = new TransformationStep(TransformationStepType.MODEL_ACCESS, modelAccessProviderClassName, modelAccessClassName, declarativeService);
				result.add(modelAccess);
				genset.addTransformationStep(modelAccess);
				if (declarativeService != null) declarativeService.setImplementationClass(modelAccess.getQualifiedProviderClassName());
				
				
				// --- model converter steps
				String[] modelConverterNamesArray = null;
				final String modelConverterNames = properties.getProperty(Key.CONVERT.getKey());
				if (modelConverterNames == null || modelConverterNames.trim().length() == 0) {
					throw new ModelConverterException("no converter specified - you need to specify at least one model converter");
				} else {
				    modelConverterNamesArray = modelConverterNames.trim().split("[,]");
				}
				
				for (int ii=0; ii<modelConverterNamesArray.length; ii++) {
					String modelConverterName = modelConverterNamesArray[ii].trim();
					final String modelConverterProviderClassName = modelConverterName != null && modelConverterName.length() > 0 ? modelConverterName+"ConverterProvider" : "";
					final String modelConverterClassName = modelConverterName != null && modelConverterName.length() > 0 ? modelConverterName+"Converter" : "";
					
					if (properties.containsKey(Key.CONVERT.getKey())) declarativeService = new DeclarativeService(TransformationStepType.MODEL_CONVERTER.getPackagePostfix()+ii+".xml", "org.jenerateit.modelconverter.ModelConverterProviderI");
					TransformationStep modelConverter = new TransformationStep(TransformationStepType.MODEL_CONVERTER, modelConverterProviderClassName, modelConverterClassName, declarativeService);
					result.add(modelConverter);
					genset.addTransformationStep(modelConverter);
					if (declarativeService != null) declarativeService.setImplementationClass(modelConverter.getQualifiedProviderClassName());
				}
				
				// --- generation group step
				final String generationGroupName = properties.getProperty(Key.GENERATOR.getKey());
				final String generationGroupProviderClassName = generationGroupName != null && generationGroupName.length() > 0 ? "GenerationGroup"+generationGroupName+"Provider" : "";
				final String generationGroupClassName = generationGroupName != null && generationGroupName.length() > 0 ? "GenerationGroup"+generationGroupName : "";
				
				if (properties.containsKey(Key.GENERATOR.getKey())) declarativeService = new DeclarativeService(TransformationStepType.GENERATION_GROUP.getPackagePostfix()+".xml", "org.jenerateit.generationgroup.GenerationGroupProviderI");
				TransformationStep generationGroup = new TransformationStep(TransformationStepType.GENERATION_GROUP, generationGroupProviderClassName, generationGroupClassName, declarativeService);
				result.add(generationGroup);
				
				for (String targetDefinitionName : extractTargetDefinitionNames(properties.getProperty(Key.WRITE.getKey()))) {
					
					// --- meta model type
					MetaModelType metaModelType = new MetaModelType(targetDefinitionName, genset);
					result.add(metaModelType);
					
					// --- target definition
					TargetDefinition targetDefinition = new TargetDefinition(targetDefinitionName+"Target", targetDefinitionName+"TargetDocument", targetDefinitionName+"Writer", targetDefinitionName, metaModelType);
					generationGroup.addTargetDefinition(targetDefinition);
					result.add(targetDefinition);
					

				}
				genset.addTransformationStep(generationGroup);
				if (declarativeService != null) declarativeService.setImplementationClass(generationGroup.getQualifiedProviderClassName());
				
				result.add(genset);
			}
		}
		
		return result;
	}
	
	/**
	 * @param targetNames
	 * @param argsNamesCollection
	 */
	private Collection<String> extractTargetDefinitionNames(String targetNames) {
		Collection<String> result = new ArrayList<>();
		
		if (targetNames != null) {
			String[] targetNamesArr = targetNames.split("[,]");
			for (String argsName : targetNamesArr) {
				result.add(argsName.trim());
			}
		}
		
		return result;
	}
	
	/**
	 * @author mmt
	 *
	 */
	private enum Key {
		
		BASE_PACKAGE ("base-package"),
		GENERATOR ("generator"),
		ACCESS ("access"),
		CONVERT ("convert"),
		WRITE ("write"),
		;
		
		private final String key;
		
		private Key(String key) {
			this.key = key;
		}

		public String getKey() {
			return key;
		}
	}
}
