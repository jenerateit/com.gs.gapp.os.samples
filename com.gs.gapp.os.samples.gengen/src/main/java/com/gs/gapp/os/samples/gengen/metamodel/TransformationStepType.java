/**
 * 
 */
package com.gs.gapp.os.samples.gengen.metamodel;

/**
 * @author mmt
 *
 */
public enum TransformationStepType {

	MODEL_ACCESS,
	MODEL_CONVERTER,
	GENERATION_GROUP,
	;
	
	/**
	 * @return
	 */
	public String getPackagePostfix() {
		String postfix = "";
		
		switch (this) {
		case GENERATION_GROUP:
			postfix = "generation";
			break;
		case MODEL_ACCESS:
			postfix = "modelaccess";
			break;
		case MODEL_CONVERTER:
			postfix = "converter";
			break;
		default:
			break;
		}
		
		return postfix;
	}
	
	public String getReadableName() {
		String readableName = "";
		
		switch (this) {
		case GENERATION_GROUP:
			readableName = "Generation Group";
			break;
		case MODEL_ACCESS:
			readableName = "Model Access";
			break;
		case MODEL_CONVERTER:
			readableName = "Model Converter";
			break;
		default:
			break;
		}
		
		return readableName;
	}
}
