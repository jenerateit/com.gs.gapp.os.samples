/**
 * 
 */
package com.gs.gapp.os.samples.gengen.metamodel;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author mmt
 *
 */
public class TransformationStep {
	
	private final TransformationStepType transformationStepType;
	private final String providerClassName;
	private final String implementationClassName;
	
	private final Set<TargetDefinition> targetDefinitions = new LinkedHashSet<>();
	
	private final DeclarativeService declarativeService;
	
	private Genset genset;

	/**
	 * @param transformationStepType
	 * @param providerClassName
	 * @param implementationClassName
	 * @param declarativeService
	 */
	public TransformationStep(TransformationStepType transformationStepType, String providerClassName, String implementationClassName, DeclarativeService declarativeService) {
		
		this.transformationStepType = transformationStepType;
		this.providerClassName = providerClassName;
		this.implementationClassName = implementationClassName;
		this.declarativeService = declarativeService;
	}

	public TransformationStepType getTransformationStepType() {
		return transformationStepType;
	}

	public String getProviderClassName() {
		return providerClassName;
	}

	public String getImplementationClassName() {
		return implementationClassName;
	}
	
	public Set<TargetDefinition> getTargetDefinitions() {
		return targetDefinitions;
	}
	
	public boolean addTargetDefinition(TargetDefinition targetDefinition) {
		boolean result = this.targetDefinitions.add(targetDefinition);
		targetDefinition.setTransformationStep(this);
		return result;
	}

	public Genset getGenset() {
		return genset;
	}

	void setGenset(Genset genset) {
		this.genset = genset;
	}
	
	public String getQualifiedPackageName() {
		String packagePostfix = transformationStepType.getPackagePostfix();
		return packagePostfix != null ? genset.getBasePackageName() + "." + packagePostfix : genset.getBasePackageName();
	}
	
	public String getQualifiedProviderClassName() {
		if (providerClassName != null && providerClassName.length() > 0) {
		    return new StringBuilder(getQualifiedPackageName()).append(".").append(providerClassName).toString();
		}
		
		return "";
	}
	
	public String getQualifiedImplementationClassName() {
		if (implementationClassName != null && implementationClassName.length() > 0) {
		    return new StringBuilder(getQualifiedPackageName()).append(".").append(implementationClassName).toString();
		}
		
		return "";
	}
	
	/**
	 * @return
	 */
	public String getPathSegmentForPackage() {
		return getQualifiedPackageName().replace(".", "/");
	}

	public DeclarativeService getDeclarativeService() {
		return declarativeService;
	}

	/**
	 * @author mmt
	 *
	 */
	public static class TargetDefinition {
		
		private final String targetClassName;
		private final String targetDocumentClassName;
		private final String writerClassName;
		private final String writerVelocityTemplateName;
		
		private final MetaModelType metaModelType;
		
		private TransformationStep transformationStep;
		
		/**
		 * @param targetClassName
		 * @param targetDocumentClassName
		 * @param writerClassName
		 * @param writerVelocityTemplateName
		 */
		public TargetDefinition(String targetClassName,
				String targetDocumentClassName, String writerClassName,
				String writerVelocityTemplateName, MetaModelType metaModelType) {
			super();
			this.targetClassName = targetClassName;
			this.targetDocumentClassName = targetDocumentClassName;
			this.writerClassName = writerClassName;
			this.writerVelocityTemplateName = writerVelocityTemplateName;
			this.metaModelType = metaModelType;
		}

		public String getTargetClassName() {
			return targetClassName;
		}

		public String getTargetDocumentClassName() {
			return targetDocumentClassName;
		}

		public String getWriterClassName() {
			return writerClassName;
		}

		public String getWriterVelocityTemplateName() {
			return writerVelocityTemplateName;
		}

		public TransformationStep getTransformationStep() {
			return transformationStep;
		}

		private void setTransformationStep(TransformationStep transformationStep) {
			this.transformationStep = transformationStep;
		}
		
        public String getQualifiedPackageNameForTargetClass() {
			return new StringBuilder(transformationStep.genset.getBasePackageName()).append(".target").toString();
		}

		public String getQualifiedTargetClassName() {
			return new StringBuilder(getQualifiedPackageNameForTargetClass()).append(".").append(targetClassName).toString();
		}
		
		public String getQualifiedTargetDocumentClassName() {
			return new StringBuilder(getQualifiedPackageNameForTargetClass()).append(".").append(targetDocumentClassName).toString();
		}
		
		public String getQualifiedWriterClassName() {
			return new StringBuilder(getQualifiedPackageNameForTargetClass()).append(".").append(writerClassName).toString();
		}
		
        

		@Override
		public String toString() {
			return "TargetDefinition [targetClassName=" + targetClassName
					+ ", targetDocumentClassName=" + targetDocumentClassName
					+ ", writerClassName=" + writerClassName
					+ ", writerVelocityTemplateName="
					+ writerVelocityTemplateName + ", transformationStep="
					+ transformationStep + "]";
		}

		public MetaModelType getMetaModelType() {
			return metaModelType;
		}
	}
}
