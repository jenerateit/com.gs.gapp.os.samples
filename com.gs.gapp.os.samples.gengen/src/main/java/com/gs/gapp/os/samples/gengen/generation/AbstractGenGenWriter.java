package com.gs.gapp.os.samples.gengen.generation;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;

public abstract class AbstractGenGenWriter extends AbstractTextWriter {
	

	public AbstractGenGenWriter() {}

	@Override
	public void transform(TargetSection ts) throws WriterException {
		VelocityEngine engine = new VelocityEngine();
        engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath"); 
        engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        
        Template template = engine.getTemplate(getTemplateName());
        
        
        VelocityContext context = new VelocityContext();
        context.put("model", getModelElement());
        
        StringWriter stringWriter = new StringWriter();
        template.merge(context, stringWriter);
        
        try {
			write(stringWriter.toString().getBytes("UTF-8"));
		} catch (UnsupportedEncodingException ex) {
            throw new WriterException("cannot convert result of velocity template to a byte array", ex);
		}
	}
	
	protected abstract String getTemplateName();
	
	protected abstract Object getModelElement();
}
