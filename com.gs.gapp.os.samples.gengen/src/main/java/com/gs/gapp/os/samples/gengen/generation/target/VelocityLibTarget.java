package com.gs.gapp.os.samples.gengen.generation.target;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTarget;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.os.samples.gengen.generation.JavaLibTargetDocument;
import com.gs.gapp.os.samples.gengen.metamodel.Genset;

public class VelocityLibTarget extends AbstractTarget<JavaLibTargetDocument> {
	
	@ModelElement
	private Genset genset;

	@Override
	protected URI getTargetURI() {
		try {
			return new URI("lib/velocity-1.7-dep.jar");
		} catch (URISyntaxException ex) {
			throw new TargetException("error in file URI", ex, this);
		}
	}
	
	
	/**
	 * @author mmt
	 *
	 */
	public static class VelocityLibWriter extends AbstractWriter {

		@Override
		public void transform(TargetSection ts) throws WriterException {
			InputStream velocityResourceInputStream = this.getClass().getResourceAsStream("/velocity-1.7-dep.jar");
			
			try {
				write( readInputStream(velocityResourceInputStream) );
			} catch (IOException e) {
				throw new WriterException("error reading velocity jar file from input stream", this.getTransformationTarget(), this);
			}
		}
		
		/**
		 * @param inputStream
		 * @return
		 * @throws IOException
		 */
		private byte[] readInputStream(InputStream inputStream) throws IOException {
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    byte[] buffer = new byte[1024];
		    int length = 0;
		    while ((length = inputStream.read(buffer)) != -1) {
		        baos.write(buffer, 0, length);
		    }
		    return baos.toByteArray();
		}
	}
}
