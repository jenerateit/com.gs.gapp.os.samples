/**
 * 
 */
package com.gs.gapp.os.samples.gengen.generation.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTextTarget;

import com.gs.gapp.os.samples.gengen.generation.AbstractGenGenWriter;
import com.gs.gapp.os.samples.gengen.generation.ManifestTargetDocument;
import com.gs.gapp.os.samples.gengen.metamodel.Genset;

/**
 * @author mmt
 *
 */
public class ManifestTarget extends AbstractTextTarget<ManifestTargetDocument> {
	
	
	@ModelElement
	private Genset genset;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.target.AbstractTarget#getTargetURI()
	 */
	@Override
	protected URI getTargetURI() {
		
		try {
		    return new URI("META-INF/MANIFEST.MF");
		} catch (URISyntaxException e) {
			// eat this up since this is not relevant since we have a hard-coded file name here
		}
		
		return null;
	}
	
	
	public static class ManifestWriter extends AbstractGenGenWriter {
		
		@ModelElement
		private Genset genset;
		
		@Override
		protected String getTemplateName() {
			return "Manifest.vm";
		}
		
		@Override
		protected Object getModelElement() {
			return genset;
		}
	}
}
