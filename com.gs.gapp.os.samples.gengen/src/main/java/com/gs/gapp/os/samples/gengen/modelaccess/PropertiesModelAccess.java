package com.gs.gapp.os.samples.gengen.modelaccess;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.jenerateit.modelaccess.MessageProviderModelAccess;
import org.jenerateit.modelaccess.ModelAccessException;
import org.jenerateit.modelaccess.ModelAccessOptions;


/**
 * @author mmt
 *
 */
public class PropertiesModelAccess extends MessageProviderModelAccess {

	@Override
	public void close() {}

	@Override
	public void init(ModelAccessOptions modelAccessOptions) {}

	@Override
	public boolean isOpen() {
		return false;
	}

	@Override
	public boolean isProgram() {
		return false;
	}

	@Override
	public Object open() {
		return null;
	}

	@Override
	public void selectElements(Collection<?> elements) {}

	@Override
	public String getDescription() {
		return "Model access to read a properties file and create an instance of java.util.Properties for it";
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.modelaccess.MessageProviderModelAccess#loadElements(java.io.InputStream, org.jenerateit.modelaccess.ModelAccessOptions)
	 */
	@Override
	protected Collection<?> loadElements(InputStream inputStream,
			ModelAccessOptions modelAccessOptions) throws ModelAccessException {
		
		try {
			List<Object> result = new ArrayList<Object>();
			final ZipInputStream zis = new ZipInputStream(inputStream);
			ZipEntry entry = null;
			while ((entry = zis.getNextEntry()) != null) {
				if (entry.getName().toLowerCase().endsWith(".properties")) {
					Properties properties = new Properties();
					String simpleName = entry.getName();
					if (simpleName.contains("/")) simpleName = entry.getName().substring(entry.getName().lastIndexOf("/")+1);
					simpleName = simpleName.replace(".properties", "");
					
					// --- overwriting the close() method in order to do nothing since the given zip input stream will be closed by the caller
					properties.load(new BufferedInputStream(zis) {
						@Override
						public void close() throws IOException {}
					});
					properties.put("name", simpleName);
					result.add(properties);
				} else {
					// ignore this entry since it doesn't represent a properties file
				}
				zis.closeEntry();
			}
			return result;
				
		} catch (IOException ex) {
			throw new ModelAccessException("Error while reading ZIP input stream content", ex);

		} catch (Throwable th) {
			throw new ModelAccessException("Error while creating the Properties object from ZIP input stream", th);
		}
	}
}
