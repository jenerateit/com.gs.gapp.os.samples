/**
 * 
 */
package com.gs.gapp.os.samples.gengen.generation.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTextTarget;
import org.jenerateit.target.TargetException;

import com.gs.gapp.os.samples.gengen.generation.AbstractGenGenWriter;
import com.gs.gapp.os.samples.gengen.generation.JavaTargetDocument;
import com.gs.gapp.os.samples.gengen.metamodel.TransformationStep.TargetDefinition;

/**
 * @author mmt
 *
 */
//@OneOff
public class TargetClassTarget extends AbstractTextTarget<JavaTargetDocument> {
	
	
	@ModelElement
	private TargetDefinition targetDefinition;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.target.AbstractTarget#getTargetURI()
	 */
	@Override
	protected URI getTargetURI() {
		StringBuilder sb = new StringBuilder("src/main/java/").append(getNamespace()).append("/").append(targetDefinition.getTargetClassName()).append(".java");
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
		}
	}
	
	/**
	 * @return
	 */
	private String getNamespace() {
		return targetDefinition.getQualifiedPackageNameForTargetClass().replace(".", "/");
	}

	
	public static class TargetClassWriter extends AbstractGenGenWriter {
		
		@ModelElement
		private TargetDefinition targetDefinition;
		
		@Override
		protected String getTemplateName() {
			return "TargetClass.vm";
		}
		
		@Override
		protected Object getModelElement() {
			return targetDefinition;
		}
	}
}
