package com.gs.gapp.os.samples.gengen.generation;

import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.os.samples.gengen.generation.target.BuildPropertiesTarget;
import com.gs.gapp.os.samples.gengen.generation.target.BuildPropertiesTarget.BuildPropertiesWriter;
import com.gs.gapp.os.samples.gengen.generation.target.DeclarativeServiceComponentTarget;
import com.gs.gapp.os.samples.gengen.generation.target.DeclarativeServiceComponentTarget.DeclarativeServiceComponentWriter;
import com.gs.gapp.os.samples.gengen.generation.target.EclipseProjectTarget;
import com.gs.gapp.os.samples.gengen.generation.target.EclipseProjectTarget.EclipseProjectWriter;
import com.gs.gapp.os.samples.gengen.generation.target.GensetTarget;
import com.gs.gapp.os.samples.gengen.generation.target.GensetTarget.GensetWriter;
import com.gs.gapp.os.samples.gengen.generation.target.ImplementationClassTarget;
import com.gs.gapp.os.samples.gengen.generation.target.ImplementationClassTarget.ImplementationClassWriter;
import com.gs.gapp.os.samples.gengen.generation.target.ManifestTarget;
import com.gs.gapp.os.samples.gengen.generation.target.ManifestTarget.ManifestWriter;
import com.gs.gapp.os.samples.gengen.generation.target.MetaModelTypeTarget;
import com.gs.gapp.os.samples.gengen.generation.target.MetaModelTypeTarget.MetaModelTypeWriter;
import com.gs.gapp.os.samples.gengen.generation.target.ProviderClassTarget;
import com.gs.gapp.os.samples.gengen.generation.target.ProviderClassTarget.ProviderClassWriter;
import com.gs.gapp.os.samples.gengen.generation.target.TargetClassTarget;
import com.gs.gapp.os.samples.gengen.generation.target.TargetClassTarget.TargetClassWriter;
import com.gs.gapp.os.samples.gengen.generation.target.TargetDocumentClassTarget;
import com.gs.gapp.os.samples.gengen.generation.target.TargetDocumentClassTarget.TargetDocumentClassWriter;
import com.gs.gapp.os.samples.gengen.generation.target.VelocityLibTarget;
import com.gs.gapp.os.samples.gengen.generation.target.VelocityLibTarget.VelocityLibWriter;
import com.gs.gapp.os.samples.gengen.generation.target.VelocityTemplateTarget;
import com.gs.gapp.os.samples.gengen.generation.target.VelocityTemplateTarget.VelocityTemplateWriter;
import com.gs.gapp.os.samples.gengen.metamodel.Genset;
import com.gs.gapp.os.samples.gengen.metamodel.MetaModelType;
import com.gs.gapp.os.samples.gengen.metamodel.TransformationStep;
import com.gs.gapp.os.samples.gengen.metamodel.TransformationStep.TargetDefinition;

/**
 * @author mmt
 *
 */
public class GenerationGroupGenGen implements GenerationGroupConfigI {

	@Override
	public String getDescription() {
		return this.getName();
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement,
			Class<? extends TargetI<?>> targetClass) {
		if (modelElement instanceof TargetDefinition) {
			if (targetClass == TargetClassTarget.class) {
			    return TargetClassWriter.class;
			} else if (targetClass == TargetDocumentClassTarget.class) {
			    return TargetDocumentClassWriter.class;
			} else if (targetClass == VelocityTemplateTarget.class) {
			    return VelocityTemplateWriter.class;
			}
		} else if (modelElement instanceof TransformationStep) {
			TransformationStep transformationStep = (TransformationStep) modelElement;
			if (targetClass == ProviderClassTarget.class && transformationStep.getProviderClassName() != null && transformationStep.getProviderClassName().length() > 0) {
				return ProviderClassWriter.class;
			} else if (targetClass == ImplementationClassTarget.class && transformationStep.getImplementationClassName() != null && transformationStep.getImplementationClassName().length() > 0) {
				return ImplementationClassWriter.class;
			} else if (targetClass == DeclarativeServiceComponentTarget.class && transformationStep.getDeclarativeService() != null) {
				return DeclarativeServiceComponentWriter.class;
			}
		} else if (modelElement instanceof Genset) {
			if (targetClass == ManifestTarget.class) {
				return ManifestWriter.class;
			} else if (targetClass == GensetTarget.class) {
				return GensetWriter.class;
			} else if (targetClass == BuildPropertiesTarget.class) {
				return BuildPropertiesWriter.class;
			} else if (targetClass == VelocityLibTarget.class) {
				return VelocityLibWriter.class;
			} else if (targetClass == EclipseProjectTarget.class) {
				return EclipseProjectWriter.class;
			}
		} else if (modelElement instanceof MetaModelType) {
			if (targetClass == MetaModelTypeTarget.class) {
				return MetaModelTypeWriter.class;
			}
		}
		
		return null;
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement, TargetI<?> targetInstance) {
		@SuppressWarnings({ "unchecked" })
		Class<? extends TargetI<?>> targetClazz = (Class<? extends TargetI<?>>) targetInstance.getClass();
		Class<? extends WriterI> writerClass = getWriterClass(modelElement, targetClazz);
		return writerClass;
	}

	@Override
	public Set<Class<? extends TargetI<?>>> getAllTargets() {
		Set<Class<? extends TargetI<?>>> result = new LinkedHashSet<>();
		result.add(TargetClassTarget.class);
		result.add(ManifestTarget.class);
		result.add(GensetTarget.class);
		result.add(ImplementationClassTarget.class);
		result.add(ProviderClassTarget.class);
		result.add(TargetDocumentClassTarget.class);
		result.add(MetaModelTypeTarget.class);
		result.add(VelocityTemplateTarget.class);
		result.add(DeclarativeServiceComponentTarget.class);
		result.add(BuildPropertiesTarget.class);
		result.add(VelocityLibTarget.class);
//		result.add(EclipseProjectTarget.class);  TODO reactivate this once JIT-59 is fixed (mmt 21-Apr-2015)
		return result;
	}
}
