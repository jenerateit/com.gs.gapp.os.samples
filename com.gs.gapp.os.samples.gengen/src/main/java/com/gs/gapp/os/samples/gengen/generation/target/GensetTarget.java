/**
 * 
 */
package com.gs.gapp.os.samples.gengen.generation.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTextTarget;

import com.gs.gapp.os.samples.gengen.generation.AbstractGenGenWriter;
import com.gs.gapp.os.samples.gengen.generation.XMLTargetDocument;
import com.gs.gapp.os.samples.gengen.metamodel.Genset;

/**
 * @author mmt
 *
 */
//@OneOff
public class GensetTarget extends AbstractTextTarget<XMLTargetDocument> {
	
	
	@ModelElement
	private Genset genset;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.target.AbstractTarget#getTargetURI()
	 */
	@Override
	protected URI getTargetURI() {
		
		try {
		    return new URI("VD-INF/virtualdeveloper.genset");
		} catch (URISyntaxException e) {
			// eat this up since this is not relevant since we have a hard-coded file name here
		}
		
		return null;
	}
	
	
	public static class GensetWriter extends AbstractGenGenWriter {
		
		@ModelElement
		private Genset genset;
		
		@Override
		protected String getTemplateName() {
			return "Genset.vm";
		}

		@Override
		protected Object getModelElement() {
			return genset;
		}
	}
}
