/**
 * 
 */
package com.gs.gapp.os.samples.gengen.generation.target;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTextTarget;
import org.jenerateit.target.TargetSection;
import org.jenerateit.target.TextTargetDocumentI;
import org.jenerateit.writer.AbstractTextWriter;
import org.jenerateit.writer.WriterException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

import com.gs.gapp.os.samples.gengen.generation.XMLTargetDocument;
import com.gs.gapp.os.samples.gengen.metamodel.Genset;

/**
 * @author mmt
 *
 */
public class EclipseProjectTarget extends AbstractTextTarget<XMLTargetDocument> {
	
	
	@ModelElement
	private Genset genset;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.target.AbstractTarget#getTargetURI()
	 */
	@Override
	protected URI getTargetURI() {
		
		try {
		    return new URI(".project");
		} catch (URISyntaxException e) {
			// eat this up since this is not relevant since we have a hard-coded file name here
		}
		
		return null;
	}
	
	
	public static class EclipseProjectWriter extends AbstractTextWriter {
		
		@ModelElement
		private Genset genset;

		@Override
		public void transform(TargetSection ts) throws WriterException {
			TextTargetDocumentI previousTargetDocument = this.getTextTransformationTarget().getPreviousTargetDocument();
			@SuppressWarnings("unused")
			String previousFileContent = new String(previousTargetDocument.toByte());
			
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db;
			try {
				db = dbf.newDocumentBuilder();
				ByteArrayInputStream byis = new ByteArrayInputStream(previousTargetDocument.toByte());
				
				if (byis.available() > 0) {
					Document doc = db.parse( byis );
					
					NodeList naturesElements = doc.getElementsByTagName("natures");
					Node naturesNode = naturesElements.item(0);
					
					if (naturesNode != null) {
						NodeList nodeList = naturesNode.getChildNodes();
						
						boolean hasGeneratorNature = false;
						boolean hasModelAccessNature = false;
						boolean hasModelConverterNature = false;
						boolean hasGenerationGroupNature = false;
						
						for (int ii=0; ii<nodeList.getLength(); ii++ ) {
							Node natureNode = nodeList.item(ii);
							if (natureNode.getNodeType() != Node.ELEMENT_NODE) continue;
							
							String natureValue = natureNode.getTextContent().trim();
							if (natureValue.equals("com.vd.jenerateit-eclipse-plugin.jenerateit_virtual_developer")) {
								hasGenerationGroupNature = true;
							} else if (natureValue.equals("com.vd.jenerateit-eclipse-plugin.jenerateit_model_access")) {
								hasModelAccessNature = true;
							} else if (natureValue.equals("com.vd.jenerateit-eclipse-plugin.jenerateit_plugin_project")) {
								hasGeneratorNature = true;
							} else if (natureValue.equals("com.vd.jenerateit-eclipse-plugin.jenerateit_model_converter")) {
								hasModelConverterNature = true;
							}
						}
						
						if (!hasGenerationGroupNature) addNature(doc, naturesNode, "com.vd.jenerateit-eclipse-plugin.jenerateit_virtual_developer");
						if (!hasModelAccessNature)     addNature(doc, naturesNode, "com.vd.jenerateit-eclipse-plugin.jenerateit_model_access");
						if (!hasGeneratorNature)       addNature(doc, naturesNode, "com.vd.jenerateit-eclipse-plugin.jenerateit_plugin_project");
						if (!hasModelConverterNature)  addNature(doc, naturesNode, "com.vd.jenerateit-eclipse-plugin.jenerateit_model_converter");
					}
						
					DOMSource domSource = new DOMSource(doc);
					StringWriter writer = new StringWriter();
					StreamResult result = new StreamResult(writer);
					TransformerFactory tf = TransformerFactory.newInstance();
					tf.setAttribute("indent-number", 4);
					Transformer transformer = tf.newTransformer();
					transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
			        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			        transformer.transform(domSource, result);
			        writer.flush();
			        
					String textContent = writer.toString();
					if (textContent != null && textContent.trim().length() > 0) {
						write( textContent.trim() );
					} else {
						write( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" );
					}
				} else {
					write( "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" );
				}
				
			} catch (ParserConfigurationException ex) {
				throw new WriterException("problems handling XML file", ex);
			} catch (SAXException ex) {
				throw new WriterException("problems handling XML file", ex);
			} catch (IOException ex) {
				throw new WriterException("problems handling XML file", ex);
			} catch (TransformerException ex) {
				throw new WriterException("problems handling XML file", ex);
			}
		}
		
		private void addNature(Document doc, Node naturesNode, String nature) {
			Element newNature = doc.createElement("nature");
			Text textNode = doc.createTextNode(nature);
			newNature.appendChild(textNode);
			naturesNode.appendChild(newNature);
		}
	}
}
