/**
 * 
 */
package com.gs.gapp.os.samples.gengen.metamodel;

/**
 * @author mmt
 *
 */
public class DeclarativeService {
	
	private final String fileName;
	private final String providedService;
	private String implementationClass;
	/**
	 * @param fileName
	 * @param providedService
	 * @param implementationClass
	 */
	public DeclarativeService(String fileName, String providedService) {
		super();
		this.fileName = fileName;
		this.providedService = providedService;
	}
	
	public String getFileName() {
		return fileName;
	}
	public String getProvidedService() {
		return providedService;
	}
	public String getImplementationClass() {
		return implementationClass;
	}

	public void setImplementationClass(String implementationClass) {
		this.implementationClass = implementationClass;
	}
}
