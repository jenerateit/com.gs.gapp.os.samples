/**
 * 
 */
package com.gs.gapp.os.samples.gengen.generation.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTextTarget;
import org.jenerateit.target.TargetException;

import com.gs.gapp.os.samples.gengen.generation.AbstractGenGenWriter;
import com.gs.gapp.os.samples.gengen.generation.JavaTargetDocument;
import com.gs.gapp.os.samples.gengen.metamodel.TransformationStep;

/**
 * @author mmt
 *
 */
public class DeclarativeServiceComponentTarget extends AbstractTextTarget<JavaTargetDocument> {
	
	
	@ModelElement
	private TransformationStep transformationStep;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.target.AbstractTarget#getTargetURI()
	 */
	@Override
	protected URI getTargetURI() {
		StringBuilder sb = new StringBuilder("OSGI-INF/").append(transformationStep.getDeclarativeService().getFileName());
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
		}
	}
	
	public static class DeclarativeServiceComponentWriter extends AbstractGenGenWriter {
		
		@ModelElement
		private TransformationStep transformationStep;
		
		@Override
		protected String getTemplateName() {
			return "DeclarativeServiceComponent.vm";
		}
		
		@Override
		protected Object getModelElement() {
			return transformationStep;
		}
	}
}
