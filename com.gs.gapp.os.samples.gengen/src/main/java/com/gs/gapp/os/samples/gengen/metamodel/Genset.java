package com.gs.gapp.os.samples.gengen.metamodel;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class Genset {

	private final String name;
	
	private final String basePackageName;
	
	private final Set<TransformationStep> transformationSteps = new LinkedHashSet<>();
	private final Set<MetaModelType> metaModelTypes = new LinkedHashSet<>();
	
	public Genset(String name, String basePackageName) {
		this.name = name;
		this.basePackageName = basePackageName;
	}

	public String getName() {
		return name;
	}

	public Set<TransformationStep> getTransformationSteps() {
		return transformationSteps;
	}
	
	public boolean addTransformationStep(TransformationStep transformationStep) {
		boolean result = this.transformationSteps.add(transformationStep);
		transformationStep.setGenset(this);
		return result;
	}

	public String getBasePackageName() {
		return basePackageName;
	}
	
	/**
	 * @return
	 */
	public TransformationStep getModelAccess() {
		for (TransformationStep step : transformationSteps) {
			if (step.getTransformationStepType() == TransformationStepType.MODEL_ACCESS) return step;
		}
		return null;
	}
	
	/**
	 * @return
	 */
	public List<TransformationStep> getModelConverters() {
		List<TransformationStep> result = new ArrayList<>();
		for (TransformationStep step : transformationSteps) {
			if (step.getTransformationStepType() == TransformationStepType.MODEL_CONVERTER) result.add(step);
		}
		return result;
	}
	
	/**
	 * @return
	 */
	public TransformationStep getGenerationGroup() {
		for (TransformationStep step : transformationSteps) {
			if (step.getTransformationStepType() == TransformationStepType.GENERATION_GROUP) return step;
		}
		return null;
	}
	
	public String getBundleId() {
		return new StringBuilder(basePackageName).append(".").append(name).toString();
	}

	public Set<MetaModelType> getMetaModelTypes() {
		return metaModelTypes;
	}
	
	boolean addMetaModelType(MetaModelType metaModelType) {
		boolean result = this.metaModelTypes.add(metaModelType);
		return result;
	}
	
	public String getServiceComponentsForManifest() {
		StringBuilder sb = new StringBuilder();
		
		for (TransformationStep step : this.transformationSteps) {
			if (step.getDeclarativeService() == null) continue;
			
			if (sb.length() > 0) sb.append(",");
			sb.append("OSGI-INF/").append(step.getDeclarativeService().getFileName());
		}
		
		return sb.toString();
	}
}
