package ${model.qualifiedPackageNameForTargetClass};

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTextTarget;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;
import org.jenerateit.writer.AbstractTextWriter;

import ${model.qualifiedTargetDocumentClassName};
import ${model.metaModelType.qualifiedClassName};

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;


/**
 *
 */
public class ${model.targetClassName} extends AbstractTextTarget<${model.targetDocumentClassName}> {
	
	
	@ModelElement
	private ${model.metaModelType.className} modelElement;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.target.AbstractTarget#getTargetURI()
	 */
	@Override
	protected URI getTargetURI() {
	    // TODO add your own path rules (directories, file ending, ...)
		StringBuilder sb = new StringBuilder("src/main/java/").append(modelElement.getName()).append(".txt");
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
		}
	}
	
	public static class ${model.writerClassName} extends AbstractTextWriter {
		
		@ModelElement
		private ${model.metaModelType.className} modelElement;
		
		protected String getTemplateName() {
			return "${model.writerVelocityTemplateName}.vm";
		}
		
		protected Object getModelElement() {
			return modelElement;
		}
		
		@Override
	    public void transform(TargetSection ts) throws WriterException {
			VelocityEngine engine = new VelocityEngine();
	        engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath"); 
	        engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
	        
	        Template template = engine.getTemplate(getTemplateName());
	        
	        
	        VelocityContext context = new VelocityContext();
	        context.put("model", getModelElement());
	        
	        StringWriter stringWriter = new StringWriter();
	        template.merge(context, stringWriter);
	        
	        try {
				write(stringWriter.toString().getBytes("UTF-8"));
			} catch (UnsupportedEncodingException ex) {
	            throw new WriterException("cannot convert result of velocity template to a byte array", ex);
			}
		}
	}
}
