<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

	<modelVersion>4.0.0</modelVersion>

	<groupId>com.gs.gapp.os.samples</groupId>
	<artifactId>com.gs.gapp.os.samples.maven</artifactId>
	<packaging>pom</packaging>
	<version>1.0.0</version>

	<name>Sample Generators Maven Aggregator</name>
	<description>Parent pom of all gApp OpenSource Sample Generators</description>

	<properties>
		<tycho-version>0.22.0</tycho-version>
		<tycho-extras.version>0.22.0</tycho-extras.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<developers>
		<developer>
			<id>hrr</id>
			<name>Heinz Rohmer</name>
			<email>heinz.rohmer@generative-software.com</email>
			<url>https://www.xing.com/profile/Heinz_Rohmer</url>
			<organization>Generative Software GmbH</organization>
			<organizationUrl>http://www.generative-software.com</organizationUrl>
			<timezone>(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</timezone>
			<roles>
				<role>developer architect</role>
			</roles>
		</developer>
		<developer>
			<id>mmt</id>
			<name>Marcus Munzert</name>
			<email>marcus.munzert@generative-software.com</email>
			<url>https://www.xing.com/profile/Marcus_Munzert</url>
			<organization>Generative Software GmbH</organization>
			<organizationUrl>http://www.generative-software.com</organizationUrl>
			<timezone>(GMT+01:00) Amsterdam, Berlin, Bern, Rome, Stockholm, Vienna</timezone>
			<roles>
				<role>architect developer</role>
			</roles>
		</developer>
	</developers>

	<licenses>
		<license>
			<name>Eclipse Public License (EPL)</name>
			<url>http://www.eclipse.org/org/documents/epl-v10.html</url>
		</license>
	</licenses>

	<modules>
		<module>com.gs.gapp.os.samples.cmdlineprog</module>
		<module>com.gs.gapp.os.samples.emfstatistics</module>
		<module>com.gs.gapp.os.samples.gengen</module>
		<module>com.gs.gapp.os.samples.helloworld</module>
		<module>com.gs.gapp.os.samples.kmltestdata</module>
		<module>com.gs.gapp.os.samples.xjc</module>
		<module>com.gs.gapp.os.samples.target</module>
		
		<module>com.gs.gapp.os.samples.pattern01</module>
		<module>com.gs.gapp.os.samples.pattern02</module>
		<module>com.gs.gapp.os.samples.pattern03</module>
		<module>com.gs.gapp.os.samples.pattern11</module>
		<module>com.gs.gapp.os.samples.pattern14</module>
		<module>com.gs.gapp.os.samples.pattern25</module>
	</modules>

	<build>
		<resources>
			<resource>
				<directory>src/main/resources</directory>
				<excludes>
					<exclude>**/rebel.xml</exclude>
					<exclude>**/.gitignore</exclude>
				</excludes>
			</resource>
		</resources>

		<plugins>
			<!-- Tycho for OSGI bundles and Eclipse plugins -->
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-versions-plugin</artifactId>
				<version>${tycho-version}</version>
				<configuration>
					<newVersion>${project.version}</newVersion>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-maven-plugin</artifactId>
				<version>${tycho-version}</version>
				<extensions>true</extensions>
			</plugin>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-compiler-plugin</artifactId>
				<version>${tycho-version}</version>
				<configuration>
					<!-- Bind Groovy Eclipse Compiler -->
					<compilerId>groovy-eclipse-compiler</compilerId>
					<excludeResources>
						<excludeResource>**/rebel.xml</excludeResource>
						<excludeResource>**/.gitignore</excludeResource>
					</excludeResources>
					<source>1.7</source>
					<target>1.7</target>
				</configuration>
				<dependencies>
					<!-- Define which Groovy version will be used for build (default is 
						2.0) -->
					<dependency>
						<groupId>org.codehaus.groovy</groupId>
						<artifactId>groovy-eclipse-batch</artifactId>
						<version>1.8.6-01</version>
					</dependency>
					<!-- Define dependency to Groovy Eclipse Compiler (as it's referred 
						in compilerId) -->
					<dependency>
						<groupId>org.codehaus.groovy</groupId>
						<artifactId>groovy-eclipse-compiler</artifactId>
						<!-- <version>2.7.0-01</version> since tycho 0.19, a newer version 
							of the groovy compiler has to be used (mmt 01-Mar-2015) -->
						<version>2.9.2-01</version>
					</dependency>
				</dependencies>
			</plugin>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-packaging-plugin</artifactId>
				<version>${tycho-version}</version>
				<dependencies>
					<dependency>
						<groupId>org.eclipse.tycho.extras</groupId>
						<artifactId>tycho-buildtimestamp-jgit</artifactId>
						<version>${tycho-extras.version}</version>
					</dependency>
				</dependencies>
				<configuration>
					<timestampProvider>jgit</timestampProvider>
					<jgit.ignore>
						pom.xml
					</jgit.ignore>
					<jgit.dirtyWorkingTree>ignore</jgit.dirtyWorkingTree>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>target-platform-configuration</artifactId>
				<version>${tycho-version}</version>
				<configuration>
					<target>
						<artifact>
							<groupId>com.gs.gapp.os.samples</groupId>
							<artifactId>com.gs.gapp.os.samples.target</artifactId>
							<version>1.0.0</version>
						</artifact>
					</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.eclipse.tycho</groupId>
				<artifactId>tycho-source-plugin</artifactId>
				<version>${tycho-version}</version>
				<executions>
					<execution>
						<id>plugin-source</id>
						<goals>
							<goal>plugin-source</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<!-- <plugin> -->
			<!-- <groupId>org.eclipse.tycho.extras</groupId> -->
			<!-- <artifactId>tycho-document-bundle-plugin</artifactId> -->
			<!-- <version>${tycho-extras.version}</version> -->
			<!-- <executions> -->
			<!-- <execution> -->
			<!-- <id>eclipse-javadoc</id> -->
			<!-- <phase>generate-resources</phase> -->
			<!-- <goals> -->
			<!-- <goal>javadoc</goal> -->
			<!-- </goals> -->
			<!-- <configuration> -->
			<!-- <javadocOptions> -->
			<!-- <ignoreError>false</ignoreError> -->
			<!-- <encoding>UTF-8</encoding> -->
			<!-- <doclet>foo.bar.MyDoclet</doclet> -->
			<!-- <docletArtifacts> -->
			<!-- <docletArtifact> -->
			<!-- <groupId>foo.bar</groupId> -->
			<!-- <artifactId>foo.bar.mydocletartifact</artifactId> -->
			<!-- <version>1.0</version> -->
			<!-- </docletArtifact> -->
			<!-- </docletArtifacts> -->
			<!-- <includes> -->
			<!-- <include>com.gs.gapp*</include> -->
			<!-- </includes> -->
			<!-- <excludes> -->
			<!-- <exclude>com.example.internal.*</exclude> -->
			<!-- </excludes> -->
			<!-- <additionalArguments> -->
			<!-- <additionalArgument>-windowtitle "The Window Title"</additionalArgument> -->
			<!-- <additionalArgument>-nosince</additionalArgument> -->
			<!-- </additionalArguments> -->
			<!-- </javadocOptions> -->
			<!-- </configuration> -->
			<!-- </execution> -->
			<!-- </executions> -->
			<!-- </plugin> -->
			<!-- Define Groovy Eclipse Compiler again and set extensions=true. Thanks 
				to this, plugin will -->
			<!-- enhance default build life cycle with an extra phase which adds additional 
				Groovy source folders -->
			<!-- It works fine under Maven 3.x, but we've encountered problems with 
				Maven 2.x -->
			<plugin>
				<groupId>org.codehaus.groovy</groupId>
				<artifactId>groovy-eclipse-compiler</artifactId>
				<version>2.9.2-01</version>
				<extensions>true</extensions>
			</plugin>
			<!-- <plugin> -->
			<!-- <groupId>org.apache.maven.plugins</groupId> -->
			<!-- <artifactId>maven-javadoc-plugin</artifactId> -->
			<!-- <version>2.10.3</version> -->
			<!-- <configuration> -->
			<!-- Default configuration for all reports -->
			<!-- <detectLinks /> -->
			<!-- <links> -->
			<!-- <link>http://download.oracle.com/javase/7/docs/api/</link> -->
			<!-- </links> -->
			<!-- <encoding>UTF-8</encoding> -->
			<!-- </configuration> -->
			<!-- <executions> -->
			<!-- <execution> -->
			<!-- <id>aggregate</id> -->
			<!-- <goals> -->
			<!-- <goal>aggregate</goal> -->
			<!-- </goals> -->
			<!-- <phase>site</phase> -->
			<!-- <configuration> -->
			<!-- Specific configuration for the aggregate report -->
			<!-- </configuration> -->
			<!-- </execution> -->
			<!-- <execution> -->
			<!-- <id>attach-javadocs</id> -->
			<!-- <phase>package</phase> -->
			<!-- <configuration> -->
			<!-- Specific configuration for the attach-javadocs -->
			<!-- </configuration> -->
			<!-- <goals> -->
			<!-- <goal>jar</goal> -->
			<!-- </goals> -->
			<!-- </execution> -->
			<!-- </executions> -->
			<!-- </plugin> -->
		</plugins>
	</build>

	<reporting>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-pmd-plugin</artifactId>
				<version>3.1</version>
				<configuration>
					<targetJdk>1.7</targetJdk>
					<sourceEncoding>UTF-8</sourceEncoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jxr-plugin</artifactId>
				<version>2.4</version>
				<configuration>
					<encoding>UTF-8</encoding>
				</configuration>
			</plugin>
			<!-- <plugin> -->
			<!-- <groupId>org.apache.maven.plugins</groupId> -->
			<!-- <artifactId>maven-javadoc-plugin</artifactId> -->
			<!-- <version>2.10.3</version> -->
			<!-- <configuration> -->
			<!-- <detectLinks /> -->
			<!-- <links> -->
			<!-- <link>http://download.oracle.com/javase/7/docs/api/</link> -->
			<!-- </links> -->
			<!-- <encoding>UTF-8</encoding> -->
			<!-- </configuration> -->
			<!-- </plugin> -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>2.12.1</version>
				<configuration>
					<encoding>UTF-8</encoding>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-report-plugin</artifactId>
				<version>2.17</version>
			</plugin>
		</plugins>
	</reporting>
</project>
