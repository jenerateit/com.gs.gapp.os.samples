target "Virtual Developer Open Source Generators"

with source configurePhase requirements

location "http://dist.springsource.org/release/GRECLIPSE/e4.6" {
	org.codehaus.groovy25.feature.feature.group lazy
	
}

location "http://download.eclipse.org/tools/orbit/downloads/drops/R20160520211859/repository/" {
    org.apache.commons.httpclient [3.1.0.v201012070820,3.1.0.v201012070820]
    org.slf4j.api [1.7.2.v20121108-1250,1.7.2.v20121108-1250]
    org.slf4j.jcl [1.7.2.v20130115-1340,1.7.2.v20130115-1340]
    javax.annotation [1.2.0.v201602091430,1.2.0.v201602091430]
    org.apache.commons.logging [1.1.1.v201101211721,1.1.1.v201101211721]
    ch.qos.logback.classic [1.0.7.v20121108-1250,1.0.7.v20121108-1250]
    ch.qos.logback.core [1.0.7.v20121108-1250,1.0.7.v20121108-1250]
    javax.validation [1.0.0.GA_v201205091237,1.0.0.GA_v201205091237]
    ch.qos.logback.slf4j [1.0.7.v201505121915,1.0.7.v201505121915]
    org.glassfish.javax.faces [2.1.18.v201304200545,2.1.18.v201304200545]
    javax.ejb [3.1.1.v201204261316,3.1.1.v201204261316]
    javax.ws.rs [2.0.1.v201504171603,2.0.1.v201504171603]
    com.google.gson [2.2.4.v201311231704,2.2.4.v201311231704]
    org.junit [4.12.0.v201504281640,4.12.0.v201504281640]
    javax.servlet [3.1.0.v201410161800,3.1.0.v201410161800]
    cdi.api [1.0.0.v201105160744,1.0.0.v201105160744]
    javax.persistence [1.0.0.v200905011740,1.0.0.v200905011740]
    javax.jms [1.1.0.v201205091237,1.1.0.v201205091237]
    org.hamcrest.text [1.1.0,1.2.0)
}

location "http://download.eclipse.org/releases/neon/" {
	org.eclipse.equinox.sdk.feature.group [3.12.0.v20170209-1843,3.12.0.v20170209-1843]
    org.eclipse.jpt.jpa.eclipselink.feature.feature.group [3.4.100.v201603180253,3.4.100.v201603180253]
    org.eclipse.emf.validation.sdk.feature.group [1.10.0.201606071713,1.10.0.201606071713]
}

location "https://developer.virtual-developer.com/p2/jackson/2.x/" {
	com.fasterxml.jackson.feature.feature.group [2.2.2,2.2.2]
}

location "https://developer.virtual-developer.com/p2/jersey/1.x/" {
	com.sun.jersey.feature.group [1.17.1,1.17.1]
}

location "https://developer.virtual-developer.com/p2/com.vd.jenerateit-api/" {
	com.vd.jenerateit-feature.feature.group lazy
}

location "https://developer.virtual-developer.com/p2/com.vd.jenerateit-modelaccess/" {
	com.vd.jenerateit-eclipse-modelaccess-emftext.feature.group lazy
	com.vd.jenerateit-eclipse-modelaccess-office.feature.group lazy
	com.vd.jenerateit-modelaccess-groovy-feature.feature.group lazy
	com.vd.jenerateit-modelaccess-openapi-feature.feature.group lazy
}

location "https://developer.virtual-developer.com/p2/com.gs.os.library/" {
	io.swagger.feature.feature.group [2.0.1,3.0.0)
	retrofit.feature.feature.group [2.2.0,3.0.0)
	okhttp.feature.feature.group [3.6.0,4.0.0)
}

location "https://developer.virtual-developer.com/p2/com.gs.gapp.language/" {
     com.gs.gapp.language.dsl.function.feature.feature.group lazy       
     com.gs.gapp.language.dsl.basic.feature.feature.group lazy 
     com.gs.gapp.language.dsl.rest.feature.feature.group lazy 
     com.gs.gapp.language.dsl.product.feature.feature.group lazy 
     com.gs.gapp.language.dsl.java.feature.feature.group lazy 
     com.gs.gapp.language.dsl.delphi.feature.feature.group lazy 
     com.gs.gapp.language.dsl.iot.feature.feature.group lazy 
     com.gs.gapp.language.dsl.mobile.feature.feature.group lazy 
     com.gs.gapp.language.dsl.ui.feature.feature.group lazy 
     com.gs.gapp.language.dsl.ejb.feature.feature.group lazy 
     com.gs.gapp.language.dsl.persistence.feature.feature.group lazy
     com.gs.gapp.language.dsl.test.feature.feature.group lazy 
     com.gs.gapp.language.feature.feature.group lazy 
}

location "https://developer.virtual-developer.com/p2/com.gs.gapp.commons" {
    com.gs.gapp.commons-feature.feature.group lazy
}

location "https://developer.virtual-developer.com/p2/com.gs.gapp.lang.oo" {
	com.gs.gapp.oo-feature.feature.group lazy
}

location "https://developer.virtual-developer.com/p2/com.gs.gapp.converter" {
    com.gs.gapp.converter-emftext-feature.feature.group lazy
}

location "https://developer.virtual-developer.com/p2/com.gs.gapp.lang.java" {
	com.gs.gapp.java-feature.feature.group lazy
}

/*
 * From here on there are p2 repository locations being used, that are not openly
 * accessible. You need credentials to be able to access them.
 */
location vd-p2-gs "https://developer.virtual-developer.com/p2/com.vd.jenerateit-ide/" {
	com.vd.jenerateit-eclipse-feature.feature.group lazy
}

location "https://developer.virtual-developer.com/p2/com.vd.jenerateit-server/" {
	com.vd.jenerateit-server-feature.feature.group lazy
}


environment JavaSE-1.8 x86_64 macosx cocoa de_DE