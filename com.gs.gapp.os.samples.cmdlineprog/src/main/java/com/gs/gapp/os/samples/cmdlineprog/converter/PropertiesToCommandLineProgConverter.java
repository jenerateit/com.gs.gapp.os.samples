/**
 * 
 */
package com.gs.gapp.os.samples.cmdlineprog.converter;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Properties;
import java.util.Set;

import org.jenerateit.modelconverter.MessageProviderModelConverter;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.os.samples.cmdlineprog.metamodel.CommandLineProg;

/**
 * @author mmt
 *
 */
public class PropertiesToCommandLineProgConverter extends MessageProviderModelConverter {
	

	/* (non-Javadoc)
	 * @see org.jenerateit.modelconverter.MessageProviderModelConverter#clientConvert(java.util.Collection, org.jenerateit.modelconverter.ModelConverterOptions)
	 */
	@Override
	protected final Set<Object> clientConvert(Collection<?> elements,
			ModelConverterOptions options) throws ModelConverterException {
		
		LinkedHashSet<Object> result = new LinkedHashSet<>();
		
		for (Object element : elements) {
			if (element instanceof Properties) {
				Properties properties = (Properties) element;
				
				CommandLineProg commandLineProg = new CommandLineProg(properties.getProperty("name"), properties.getProperty("namespace"));
				extractArgsNames( properties.getProperty("simple-args"),    commandLineProg.getOptionNamesForSimpleArgs()   );
				extractArgsNames( properties.getProperty("key-value-args"), commandLineProg.getOptionNamesForKeyValueArgs() );
				extractArgsNames( properties.getProperty("switch-args"),    commandLineProg.getOptionNamesForSwitchArgs()   );
				
				result.add(commandLineProg);
			}
		}
		
		return result;
	}
	
	/**
	 * @param argsList
	 * @param argsNamesCollection
	 */
	private void extractArgsNames(String argsList, LinkedHashSet<String> argsNamesCollection) {
		String[] argsNames = null;
		
		if (argsList != null && argsNamesCollection != null) {
			argsNames = argsList.split("[, ]");
			for (String argsName : argsNames) {
				argsName = argsName.trim();
				if (argsName != null && argsName.length() > 0) {
					argsNamesCollection.add(argsName);
				}
			}
		}
	}
}
