/**
 * 
 */
package com.gs.gapp.os.samples.cmdlineprog.generation;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTarget;
import org.jenerateit.target.TargetException;

import com.gs.gapp.os.samples.cmdlineprog.metamodel.CommandLineProg;

/**
 * @author mmt
 *
 */
public class CommandLineProgTarget extends AbstractTarget<CommandLineProgTargetDocument> {
	
	
	@ModelElement
	private CommandLineProg commandLineProg;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.target.AbstractTarget#getTargetURI()
	 */
	@Override
	protected URI getTargetURI() {
		StringBuilder sb = new StringBuilder(getSourcePath()).append("/").append(getNamespace()).append(commandLineProg.getName()).append(".java");
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
		}
	}
	
	/**
	 * @return
	 */
	private String getNamespace() {
		return (commandLineProg.getNamespace() == null || commandLineProg.getNamespace().length() == 0) ? "" : (commandLineProg.getNamespace()+"/");
	}

	/**
	 * @return
	 */
	private String getSourcePath() {
		String sourcePath = (String) getGenerationGroup().getOption("source-path");
		return sourcePath == null || sourcePath.length() == 0 ? "." : sourcePath;
	}
}
