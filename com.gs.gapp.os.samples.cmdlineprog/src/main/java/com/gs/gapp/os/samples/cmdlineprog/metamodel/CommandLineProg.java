/**
 * 
 */
package com.gs.gapp.os.samples.cmdlineprog.metamodel;

import java.util.LinkedHashSet;

/**
 * @author mmt
 *
 */
public class CommandLineProg {

	private final String name;
	private final String namespace;
	
	private final LinkedHashSet<String> optionNamesForSimpleArgs = new LinkedHashSet<>();
	private final LinkedHashSet<String> optionNamesForKeyValueArgs = new LinkedHashSet<>();
	private final LinkedHashSet<String> optionNamesForSwitchArgs = new LinkedHashSet<>();
	
	/**
	 * @param name
	 * @param namespace
	 */
	public CommandLineProg(String name, String namespace) {
		super();
		this.name = name;
		this.namespace = namespace;
	}
	
	public String getName() {
		return name;
	}
	
	public String getNamespace() {
		return namespace;
	}

	public LinkedHashSet<String> getOptionNamesForSimpleArgs() {
		return optionNamesForSimpleArgs;
	}
	
	public boolean addOptionNameForSimpleArgs(String optionName) {
		return this.optionNamesForSimpleArgs.add(optionName);
	}

	public LinkedHashSet<String> getOptionNamesForKeyValueArgs() {
		return optionNamesForKeyValueArgs;
	}
	
	public boolean addOptionNameForKeyValueArgs(String optionName) {
		return this.optionNamesForKeyValueArgs.add(optionName);
	}

	public LinkedHashSet<String> getOptionNamesForSwitchArgs() {
		return optionNamesForSwitchArgs;
	}
	
	public boolean addOptionNameForSwitchArgs(String optionName) {
		return this.optionNamesForSwitchArgs.add(optionName);
	}
}
