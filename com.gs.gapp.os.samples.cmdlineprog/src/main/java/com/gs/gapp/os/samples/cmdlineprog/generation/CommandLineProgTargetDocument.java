/**
 * 
 */
package com.gs.gapp.os.samples.cmdlineprog.generation;

import java.util.Arrays;
import java.util.SortedSet;
import java.util.TreeSet;

import org.jenerateit.target.AbstractTextTargetDocument;
import org.jenerateit.target.TargetSection;

/**
 * @author mmt
 *
 */
public class CommandLineProgTargetDocument extends AbstractTextTargetDocument {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2369637221123399820L;

	public static final TargetSection DOCUMENT = new TargetSection("document", 10);

	private static final SortedSet<TargetSection> SECTIONS = new TreeSet<TargetSection>(
			Arrays.asList(new TargetSection[] {
					DOCUMENT
			}));

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TextTargetDocumentI#getCommentEnd()
	 */
	@Override
	public CharSequence getCommentEnd() {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TextTargetDocumentI#getCommentStart()
	 */
	@Override
	public CharSequence getCommentStart() {
		return "//";
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TextTargetDocumentI#getPrefixChar()
	 */
	@Override
	public char getPrefixChar() {
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.jenerateit.target.TargetDocumentI#getTargetSections()
	 */
	@Override
	public SortedSet<TargetSection> getTargetSections() {
		return SECTIONS;
	}

}
