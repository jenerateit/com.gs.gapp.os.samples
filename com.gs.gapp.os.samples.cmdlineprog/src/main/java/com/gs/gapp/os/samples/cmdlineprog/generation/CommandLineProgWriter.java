/**
 * 
 */
package com.gs.gapp.os.samples.cmdlineprog.generation;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.AbstractWriter;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.os.samples.cmdlineprog.metamodel.CommandLineProg;

/**
 * @author mmt
 *
 */
public class CommandLineProgWriter extends AbstractWriter {
	
	@ModelElement
	private CommandLineProg commandLineProg;

	/* (non-Javadoc)
	 * @see org.jenerateit.writer.WriterI#transform(org.jenerateit.target.TargetSection)
	 */
	@Override
	public void transform(TargetSection ts) throws WriterException {
        VelocityEngine engine = new VelocityEngine();
        engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath"); 
        engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        
        Template template = engine.getTemplate("main.vm");
        
        VelocityContext context = new VelocityContext();
        context.put("model", commandLineProg);
        
        StringWriter stringWriter = new StringWriter();
        template.merge(context, stringWriter);
        
        try {
			write(stringWriter.toString().getBytes("UTF-8"));
		} catch (UnsupportedEncodingException ex) {
            throw new WriterException("cannot convert result of velocity template to a byte array", ex);
		}
	}
}
