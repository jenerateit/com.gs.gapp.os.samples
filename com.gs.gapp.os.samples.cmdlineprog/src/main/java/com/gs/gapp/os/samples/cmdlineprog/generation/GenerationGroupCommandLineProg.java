package com.gs.gapp.os.samples.cmdlineprog.generation;

import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.os.samples.cmdlineprog.metamodel.CommandLineProg;

/**
 * @author mmt
 *
 */
public class GenerationGroupCommandLineProg implements GenerationGroupConfigI {

	@Override
	public String getDescription() {
		return this.getName();
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement,
			Class<? extends TargetI<?>> targetClass) {
		if (modelElement instanceof CommandLineProg) {
			return CommandLineProgWriter.class;
		}
		return null;
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement, TargetI<?> targetInstance) {
		return CommandLineProgWriter.class;
	}

	@Override
	public Set<Class<? extends TargetI<?>>> getAllTargets() {
		Set<Class<? extends TargetI<?>>> result = new LinkedHashSet<>();
		result.add(CommandLineProgTarget.class);
		return result;
	}
}
