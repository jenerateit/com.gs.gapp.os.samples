/**
 * 
 */
package com.gs.gapp.os.samples.cmdlineprog.generation;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupProviderI;
import org.osgi.service.component.ComponentContext;

/**
 * @author mmt
 *
 */
public class GenerationGroupCommandLineProgProvider implements GenerationGroupProviderI {

	private ComponentContext context;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupProviderI#getGenerationGroupConfig()
	 */
	@Override
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupCommandLineProg();
	}
	
	public void activate(final ComponentContext context) {
		this.context = context;
	}
	
	public void deactivate() {
		this.context = null;
	}

	public ComponentContext getContext() {
		return context;
	}

	public void setContext(ComponentContext context) {
		this.context = context;
	}
}
