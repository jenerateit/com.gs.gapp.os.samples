package com.gs.gapp.os.samples.emfstatistics.target;

import java.net.URI;
import java.net.URISyntaxException;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.AbstractTextTarget;
import org.jenerateit.target.TargetException;
import org.jenerateit.target.TargetSection;
import org.jenerateit.writer.WriterException;

import com.gs.gapp.os.samples.emfstatistics.metamodel.Statistics;
import com.gs.gapp.os.samples.emfstatistics.target.StatisticsTargetDocument;

import org.jenerateit.writer.AbstractTextWriter;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;


/**
 *
 */
public class StatisticsTarget extends AbstractTextTarget<StatisticsTargetDocument> {
	
	
	@ModelElement
	private Statistics modelElement;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.target.AbstractTarget#getTargetURI()
	 */
	@Override
	protected URI getTargetURI() {
	    // TODO add your own path rules (directories, file ending, ...)
		StringBuilder sb = new StringBuilder("src/main/java/").append(modelElement.getName()).append(".txt");
		
		try {
		    return new URI(sb.toString());
		} catch (URISyntaxException e) {
			throw new TargetException("Error while creating target URI for file path " + sb.toString(), e, this);
		}
	}
	
	public static class StatisticsWriter extends AbstractTextWriter {
		
		@ModelElement
		private Statistics modelElement;
		
		protected String getTemplateName() {
			return "Statistics.vm";
		}
		
		protected Object getModelElement() {
			return modelElement;
		}
		
		@Override
	    public void transform(TargetSection ts) throws WriterException {
			VelocityEngine engine = new VelocityEngine();
	        engine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath"); 
	        engine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
	        
	        Template template = engine.getTemplate(getTemplateName());
	        
	        
	        VelocityContext context = new VelocityContext();
	        context.put("model", getModelElement());
	        
	        StringWriter stringWriter = new StringWriter();
	        template.merge(context, stringWriter);
	        
	        try {
				write(stringWriter.toString().getBytes("UTF-8"));
			} catch (UnsupportedEncodingException ex) {
	            throw new WriterException("cannot convert result of velocity template to a byte array", ex);
			}
		}
	}
}
