package com.gs.gapp.os.samples.emfstatistics.converter;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.jenerateit.modelconverter.MessageProviderModelConverter;
import org.jenerateit.modelconverter.ModelConverterException;
import org.jenerateit.modelconverter.ModelConverterOptions;

import com.gs.gapp.os.samples.emfstatistics.metamodel.Statistics;

/**
 *
 */
public class EMFToStatisticsConverter extends MessageProviderModelConverter {
	

	
	@Override
	protected final Set<Object> clientConvert(Collection<?> elements,
			ModelConverterOptions options) throws ModelConverterException {
		
		LinkedHashSet<Object> result = new LinkedHashSet<>();
		Statistics statistics = new Statistics("classes");
		
		for (Object element : elements) {
			if (element instanceof EObject) {
				EObject obj = (EObject) element;
				statistics.incrementNumberOfOccurrences(obj.eClass().getName());
			}
		}
		
		result.add(statistics);
		
		return result;
	}
}
