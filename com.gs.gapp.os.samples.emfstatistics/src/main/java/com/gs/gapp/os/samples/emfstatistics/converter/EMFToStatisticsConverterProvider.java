package com.gs.gapp.os.samples.emfstatistics.converter;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;
import org.osgi.service.component.ComponentContext;

/**
 *
 */
public class EMFToStatisticsConverterProvider implements ModelConverterProviderI {

	private ComponentContext context;
	
	@Override
	public ModelConverterI getModelConverter() {
		return new EMFToStatisticsConverter();
	}
	
	public void activate(final ComponentContext context) {
		this.context = context;
	}
	
	public void deactivate() {
		this.context = null;
	}

	public ComponentContext getContext() {
		return context;
	}

	public void setContext(ComponentContext context) {
		this.context = context;
	}
}
