package com.gs.gapp.os.samples.emfstatistics.metamodel;

import java.util.LinkedHashMap;
import java.util.Map;

public class Statistics {

    private final String name;
    
    private final Map<String, Integer> classNamesAndNumberOfOccurrences = new LinkedHashMap<>();
    
    public Statistics(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

	public Map<String, Integer> getClassNamesAndNumberOfOccurrences() {
		return classNamesAndNumberOfOccurrences;
	}
	
	public void incrementNumberOfOccurrences(String className) {
		if (classNamesAndNumberOfOccurrences.containsKey(className) == false) {
			classNamesAndNumberOfOccurrences.put(className, 1);
		} else {
			Integer count = classNamesAndNumberOfOccurrences.get(className);
			classNamesAndNumberOfOccurrences.put(className, count.intValue()+1);
		}
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for (String className : classNamesAndNumberOfOccurrences.keySet()) {
			if (sb.length() > 0) sb.append(System.getProperty("line.separator"));
			sb.append(className).append(":").append(classNamesAndNumberOfOccurrences.get(className));
		}
		
		return sb.toString();
	}
}