package com.gs.gapp.os.samples.emfstatistics.generation;

import java.util.LinkedHashSet;
import java.util.Set;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.target.TargetI;
import org.jenerateit.writer.WriterI;

import com.gs.gapp.os.samples.emfstatistics.metamodel.Statistics;
import com.gs.gapp.os.samples.emfstatistics.target.StatisticsTarget;
import com.gs.gapp.os.samples.emfstatistics.target.StatisticsTarget.StatisticsWriter;

/**
 *
 */
public class GenerationGroupEMFStatistics implements GenerationGroupConfigI {

	@Override
	public String getDescription() {
		return this.getName();
	}

	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement,
			Class<? extends TargetI<?>> targetClass) {
        if (modelElement instanceof Statistics) {
            return StatisticsWriter.class;
        }
		
		return null;
	}

	@Override
	public Class<? extends WriterI> getWriterClass(Object modelElement, TargetI<?> targetInstance) {
		@SuppressWarnings({ "unchecked" })
		Class<? extends TargetI<?>> targetClazz = (Class<? extends TargetI<?>>) targetInstance.getClass();
		Class<? extends WriterI> writerClass = getWriterClass(modelElement, targetClazz);
		return writerClass;
	}

	@Override
	public Set<Class<? extends TargetI<?>>> getAllTargets() {
		Set<Class<? extends TargetI<?>>> result = new LinkedHashSet<>();
        result.add(StatisticsTarget.class);
		return result;
	}
}
