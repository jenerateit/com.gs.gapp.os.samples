package com.gs.gapp.os.samples.pattern02;

import org.jenerateit.generationgroup.WriterLocatorI;

import com.gs.gapp.generation.basic.AbstractGenerationGroup;
import com.gs.gapp.generation.basic.AbstractWriterLocator;
import com.gs.gapp.generation.basic.WriterMapper;
import com.gs.gapp.generation.java.target.JavaClassTarget;
import com.gs.gapp.generation.java.target.JavaInterfaceTarget;
import com.gs.gapp.generation.java.target.JavaTarget;
import com.gs.gapp.generation.java.writer.JavaClassWriter;
import com.gs.gapp.generation.java.writer.JavaInterfaceWriter;
import com.gs.gapp.generation.java.writer.JavaPackageWriter;
import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.java.JavaClass;
import com.gs.gapp.metamodel.java.JavaInterface;
import com.gs.gapp.metamodel.java.JavaPackage;

/**
 *
 */
public class GenerationGroupPattern02 extends AbstractGenerationGroup {

	private final WriterLocatorI writerLocator;

	public GenerationGroupPattern02(ModelElementCache modelElementCache) {
		super(modelElementCache);
		writerLocator = new Pattern01WriterLocator(this);
	}

	@Override
	public WriterLocatorI getWriterLocator() {
		return writerLocator;
	}
	
	
	/**
	 * @author mmt
	 *
	 */
	public static class Pattern01WriterLocator extends AbstractWriterLocator {

		public Pattern01WriterLocator(AbstractGenerationGroup generationGroup) {
			super(generationGroup);
			
			// --- add writer mapper for decisions
			addWriterMapperForGenerationDecision(new WriterMapper(JavaClass.class, JavaClassTarget.class, JavaClassWriter.class));
			addWriterMapperForGenerationDecision(new WriterMapper(JavaInterface.class, JavaInterfaceTarget.class, JavaInterfaceWriter.class));
			
			// --- add writer mapper for delegations
			addWriterMapperForWriterDelegation(new WriterMapper(JavaPackage.class, JavaTarget.class, JavaPackageWriter.class));
		}
	}
}
