package com.gs.gapp.os.samples.pattern01;

import org.jenerateit.modelconverter.ModelConverterI;
import org.jenerateit.modelconverter.ModelConverterProviderI;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.gs.gapp.library.converter.JavaModelElementCache;
import com.gs.gapp.library.converter.JavaToElementMapperI;

/**
 *
 */
@Component()
public class ModelConverterPattern01Provider implements ModelConverterProviderI {

	private ComponentContext context;
	private JavaToElementMapperI javaToElementMapper;
	
	@Override
	public ModelConverterI getModelConverter() {
		return new ModelConverterPattern01(new JavaModelElementCache(javaToElementMapper));
	}
	
	public void activate(final ComponentContext context) {
		this.context = context;
	}
	
	public void deactivate() {
		this.context = null;
	}

	public ComponentContext getContext() {
		return context;
	}

	public void setContext(ComponentContext context) {
		this.context = context;
	}
	
	public JavaToElementMapperI getJavaToElementMapper() {
		return javaToElementMapper;
	}

	@Reference()
	public void setJavaToElementMapper(JavaToElementMapperI javaToElementMapper) {
		this.javaToElementMapper = javaToElementMapper;
	}
	
	public void unsetJavaToElementMapper(JavaToElementMapperI javaToElementMapper) {
		this.javaToElementMapper = javaToElementMapper;
	}
}
