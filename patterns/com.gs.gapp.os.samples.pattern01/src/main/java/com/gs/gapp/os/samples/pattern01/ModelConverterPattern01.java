package com.gs.gapp.os.samples.pattern01;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.java.JavaClass;
import com.gs.gapp.metamodel.java.JavaPackage;

/**
 *
 */
public class ModelConverterPattern01 extends AbstractConverter {

	public ModelConverterPattern01(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		ArrayList<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = new ArrayList<>();
		result.add( new ComplexTypeToClassConverter<>(this) );
		return result;
	}
	
	/**
	 * @author mmt
	 *
	 * @param <S>
	 * @param <T>
	 */
	public static class ComplexTypeToClassConverter<S extends ComplexType, T extends JavaClass> extends AbstractM2MModelElementConverter<S, T> {

		public ComplexTypeToClassConverter(AbstractConverter modelConverter) {
			super(modelConverter, ModelElementConverterBehavior.DEFAULT);
		}

		@Override
		protected T onCreateModelElement(ComplexType type, ModelElementI previousResultingModelElement) {
			JavaPackage javaPackage = new JavaPackage("pattern01", null);
			@SuppressWarnings("unchecked")
			T result = (T) new JavaClass(type.getName(), javaPackage);
			return result;
		}
	}
}
