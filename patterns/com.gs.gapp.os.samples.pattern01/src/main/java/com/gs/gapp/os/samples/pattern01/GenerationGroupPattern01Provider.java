package com.gs.gapp.os.samples.pattern01;

import org.jenerateit.generationgroup.GenerationGroupConfigI;
import org.jenerateit.generationgroup.GenerationGroupProviderI;
import org.osgi.service.component.ComponentContext;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

import com.gs.gapp.library.converter.JavaModelElementCache;
import com.gs.gapp.library.converter.JavaToElementMapperI;

/**
 *
 */
@Component
 public class GenerationGroupPattern01Provider implements GenerationGroupProviderI {

	private ComponentContext context;
	private JavaToElementMapperI javaToElementMapper;
	
	/* (non-Javadoc)
	 * @see org.jenerateit.generationgroup.GenerationGroupProviderI#getGenerationGroupConfig()
	 */
	@Override
	public GenerationGroupConfigI getGenerationGroupConfig() {
		return new GenerationGroupPattern01(new JavaModelElementCache(javaToElementMapper));
	}
	
	public void activate(final ComponentContext context) {
		this.context = context;
	}
	
	public void deactivate() {
		this.context = null;
	}

	public ComponentContext getContext() {
		return context;
	}

	public void setContext(ComponentContext context) {
		this.context = context;
	}

	public JavaToElementMapperI getJavaToElementMapper() {
		return javaToElementMapper;
	}

	@Reference()
	public void setJavaToElementMapper(JavaToElementMapperI javaToElementMapper) {
		this.javaToElementMapper = javaToElementMapper;
	}
	
	public void unsetJavaToElementMapper(JavaToElementMapperI javaToElementMapper) {
		this.javaToElementMapper = javaToElementMapper;
	}
}
