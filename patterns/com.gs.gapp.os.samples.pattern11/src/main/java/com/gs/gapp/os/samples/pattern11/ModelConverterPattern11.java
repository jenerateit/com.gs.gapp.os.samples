package com.gs.gapp.os.samples.pattern11;

import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.java.JavaClass;
import com.gs.gapp.metamodel.java.JavaInterface;
import com.gs.gapp.metamodel.java.JavaModifier;
import com.gs.gapp.metamodel.java.JavaPackage;

/**
 *
 */
public class ModelConverterPattern11 extends AbstractConverter {

	public ModelConverterPattern11(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		ArrayList<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = new ArrayList<>();
		result.add( new ComplexTypeToClassConverter<>(this) );
		result.add( new ClassToInterfaceConverter<>(this) );
		return result;
	}
	
	/**
	 * @author mmt
	 *
	 * @param <S>
	 * @param <T>
	 */
	public static class ComplexTypeToClassConverter<S extends ComplexType, T extends JavaClass> extends AbstractM2MModelElementConverter<S, T> {

		public ComplexTypeToClassConverter(AbstractConverter modelConverter) {
			super(modelConverter, ModelElementConverterBehavior.DEFAULT);
		}

		/* (non-Javadoc)
		 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
		 */
		@Override
		protected T onCreateModelElement(ComplexType type, ModelElementI previousResultingModelElement) {
			JavaPackage javaPackage = new JavaPackage("pattern11", null);
			@SuppressWarnings("unchecked")
			T result = (T) new JavaClass(type.getName(), javaPackage);
			result.setGenerated(false);  // the Java class will not be generated due to this
			return result;
		}

		/* (non-Javadoc)
		 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElementI, com.gs.gapp.metamodel.basic.ModelElementI)
		 */
		@Override
		protected void onConvert(S type, T javaClass) {
			super.onConvert(type, javaClass);
			
			// --- this call lets the conversion framework identify an execute an appropriate element converter
			convertWithOtherConverter(JavaInterface.class, javaClass);
			// the Java interface will not be generated since the Java class' generated flag is false
		}
	}
	
	/**
	 * @author mmt
	 *
	 * @param <S>
	 * @param <T>
	 */
	public static class ClassToInterfaceConverter<S extends JavaClass, T extends JavaInterface> extends AbstractM2MModelElementConverter<S, T> {

		public ClassToInterfaceConverter(AbstractConverter modelConverter) {
			super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
		}

		@Override
		protected T onCreateModelElement(JavaClass javaClass, ModelElementI previousResultingModelElement) {
			@SuppressWarnings("unchecked")
			T result = (T) new JavaInterface(javaClass.getName() + "I", 0 | JavaModifier.PUBLIC, javaClass.getJavaPackage());
			javaClass.addParentInterface(result);
			return result;
		}
	}
}
