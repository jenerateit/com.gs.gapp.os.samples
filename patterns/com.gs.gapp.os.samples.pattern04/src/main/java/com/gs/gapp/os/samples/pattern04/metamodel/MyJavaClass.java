/**
 * 
 */
package com.gs.gapp.os.samples.pattern04.metamodel;

import com.gs.gapp.metamodel.java.JavaClass;
import com.gs.gapp.metamodel.java.JavaPackage;

/**
 * @author mmt
 *
 */
public class MyJavaClass extends JavaClass {

	private static final long serialVersionUID = 1L;

	public MyJavaClass(String name, JavaPackage aPackage) {
		super(name, aPackage);
	}
}
