/**
 * 
 */
package com.gs.gapp.os.samples.pattern04.generation;

import org.jenerateit.annotation.ModelElement;
import org.jenerateit.target.TargetSection;

import com.gs.gapp.generation.java.target.JavaClassTarget;
import com.gs.gapp.generation.java.writer.JavaMethodWriter;
import com.gs.gapp.metamodel.java.JavaMethod;
import com.gs.gapp.os.samples.pattern04.metamodel.MyJavaClass;

/**
 * @author mmt
 *
 */
public class MyJavaClassTarget extends JavaClassTarget {

	@ModelElement
	private MyJavaClass myJavaClass;
	
	/**
	 * @author mmt
	 *
	 */
	public static class MyJavaMethodWriter extends JavaMethodWriter {
		
		@ModelElement
		private JavaMethod javaMethod;

		@Override
		protected JavaMethodWriter wOperationBody(TargetSection ts) {
			
			wNL("System.out.println(\"", javaMethod.getOwningJavaType().getName(), ".", javaMethod.getName(), "()\");");
			
			return super.wOperationBody(ts);
		}
	}
}
