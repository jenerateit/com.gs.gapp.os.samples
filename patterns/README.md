# The How-To Catalog #

The how-to catalog includes sample generators where each of them shows you how to accomplish the implementation of some code generator logic. This catalog might help you when you have questions like "Hmmm! How could I solve this problem?". You can also flip through the how-tos in order to get new ideas on what you could do in your generators.

Most of the samples use a tiny textual input model that uses the Basic DSL and only has a handful model elements, similar to this one:


```
namespace com.gs.gapp.os.samples;

module Model kind = Basic;

enumeration ApplicationType {
    entry MOBILE;
    entry DESKTOP;
    entry WEB;
    entry EMBEDDED;
    entry DEEPLY_EMBEDDED;
}

type Application {
    field name : string;
    field version : uint32;
    field type : ApplicationType;
    field owner : Owner;
}

type Owner {
    field name : string;
}
```

### How-To generate a Java class (01) ###
General use of a model converter and a generation group to create a Java class.<br/>

**Highlighted gApp classes:** AbstractConverter, AbstractM2MModelElementConverter, JavaClass, AbstractGenerationGroup

### How-To generate a file dependent on another to-be generated file (02) ###
Generate a Java class that implements an interface. The interface's content is driven by the Java class' to-be-generated content.<br/>

**Highlighted gApp classes:** JavaClass, JavaInterface<br/>
**Highlighted gApp methods:** AbstractModelElementConverter#convertWithOtherConverter

### How-To add a model converter option (03) ###
Add an option to a model converter to be able to control whether a Java interface is generated or not.
This how-to also provides you with rules for the naming of such options.

**Highlighted gApp classes:** ConverterOptions

### How-To add a meta-type to become more specific than JavaClass (04) ###
In the previous how-tos the meta-types JavaClass and JavaInterface have been used. In this
how-to, a new meta-type MyJavaClass is invented, along with additional generation group functionality
to generated code inside method blocks.<br/>

**Highlighted gApp classes:** JavaClass, JavaTarget, JavaMethodWriter

### How-To add a generation group option (05) ###
Add an option to a generation group to be able to control whether more specific code is generated
inside method blocks or not.<br/>

**Highlighted gApp classes:** GenerationGroupOptions

### How-To add model validation (06) ###
A generator might not be able to process all kinds of syntactically correct input models.
In such cases, validation rules have to be implemented in the generator. And in the negative
cases, the user has to be informed about the reasons, why the generation fails.<br/>

**Highlighted gApp classes:** ModelValidatorI, ModelConverterException, WriterException  
**Highlighted gApp methods:** AbstractModelElementConverter#addError

### How-To generate a Java field of a type where that type itself is not generated (07) ###
Generated code can have references to Java types that are not generated (e.g. JDK classes).
Such types can still be used for fields, method parameters, interface implementations and inheritance.
This how-to shows you how you can accomplish this.<br/>

**Highlighted gApp methods:** JavaClass#setGenerated

### How-To collect names of to-be-generated files and write them to a file (08) ###
TODO

### How-To find model elements that got created in a different model converter (09) ###
When you are in the middle of the execution of a model converter, there are other model converters that already
have completed their job. Those other model converters can either be further up in the transformation step tree
or they can be in a parallel branch of that tree. There are two different mechanisms to find out what had been
done in the other model converters. 

### How-To generate a big number of lines of code that have only very few variable parts (10) ###
TODO

### How-To exclude files from being generated (11) ###
TODO

**Highlighted gApp methods:** JavaClass#setGenerated

### How-To generate a set of variants of one of the same file (12) ###
Sometimes you want to generate a set of files that all serve the same purpose but
have slightly different file names. And the generation of all those files should
be triggered by one and the same model element.

**Highlighted gApp classes:** org.jenerateit.annotation.Context

### How-To avoid infinite loops inside a model converter (13) ###
In complex model converter implementations it is possible to introduce infinite loops
while converting several model elements. The gApp model conversion framework provides
functionality to enable you to prevent infinite loops.

**Highlighted gApp methods:** AbstractModelElementConverter#onCreateModelElement, AbstractModelElementConverter#onConvert

### How-To allow a developer to see which model element led to the generation of which file (14) ###
To better understand how modeling affects the generated code, gApp provides a mechanism to visualize
the relation between them by providing diagrams out-of-the-box.

**Highlighted gApp classes:** AbstractAnalyticsConverter, GenerationGroupAnalyticsConverterTree, GenerationGroupAnalyticsElementTree, TargetModelizer

### How-To generate an XML file (15) ###

### How-To generate a textual or binary "hard-coded" file (16) ###

**Highlighted gApp classes:** GenerationGroupAnyFile, AnyFileTarget, AnyBinaryFileTarget

### How-To generate an OSGi manifest file (17) ###

### How-To generate a Java properties file (18) ###

### How-To limit the number of types of model elements to be handled in a generation group (19) ###

### How-To avoid string duplication for Java method and field names in element converters and writers (20) ###

### How-To develop a model converter that filters the input model elements before processing them any further (21) ###
There are cases, when a model converter should not process all model elements that are provided as input for that model converter.
This how-to show how you can use the gApp-API to do exactly this.

**Highlighted gApp classes:** ModelFilterI, 
**Highlighted gApp methods:** AbstractConverter#onPerformElementFilter

### How-To get a writer for a given model element, to find out what would be written for it (22) ###

### How-To find out, which model converter and model element converter had been creating a model element (24) ###

### How-To set the body of a Java method in a model element converter (25) ###

### How-To reuse model element converters in different model converters (26) ###

### How-To reuse writers in different generation groups (27) ###

### How-To use a non-OSGi jar file in a model converter (28) ###

### How-To access a transformation step option from within another transformation step (29) ###

### How-To use model converters as a switch (30) ###

### How-To share model converter options between a set of model converters (31) ###

