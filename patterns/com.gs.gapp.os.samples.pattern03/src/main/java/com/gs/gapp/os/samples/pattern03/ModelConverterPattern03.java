package com.gs.gapp.os.samples.pattern03;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.gs.gapp.metamodel.basic.ModelElementCache;
import com.gs.gapp.metamodel.basic.ModelElementI;
import com.gs.gapp.metamodel.basic.options.ConverterOptions;
import com.gs.gapp.metamodel.basic.typesystem.ComplexType;
import com.gs.gapp.metamodel.converter.AbstractConverter;
import com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter;
import com.gs.gapp.metamodel.converter.AbstractModelElementConverter;
import com.gs.gapp.metamodel.converter.ModelElementConverterBehavior;
import com.gs.gapp.metamodel.java.JavaClass;
import com.gs.gapp.metamodel.java.JavaInterface;
import com.gs.gapp.metamodel.java.JavaModifier;
import com.gs.gapp.metamodel.java.JavaPackage;

/**
 *
 */
public class ModelConverterPattern03 extends AbstractConverter {
	
	private ModelConverterOptions converterOptions;

	public ModelConverterPattern03(ModelElementCache modelElementCache) {
		super(modelElementCache);
	}
	
	/* (non-Javadoc)
	 * @see com.gs.gapp.converter.analytics.AbstractAnalyticsConverter#onInitOptions()
	 */
	@Override
	protected void onInitOptions() {
		this.converterOptions = new ModelConverterOptions(getOptions());
	}
	
	public ModelConverterOptions getConverterOptions() {
		return converterOptions;
	}

	/* (non-Javadoc)
	 * @see com.gs.gapp.metamodel.converter.AbstractConverter#onGetAllModelElementConverters()
	 */
	@Override
	protected List<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> onGetAllModelElementConverters() {
		ArrayList<AbstractModelElementConverter<? extends Object, ? extends ModelElementI>> result = new ArrayList<>();
		result.add( new ComplexTypeToClassConverter<>(this) );
		result.add( new ClassToInterfaceConverter<>(this) );
		return result;
	}
	
	/**
	 * @author mmt
	 *
	 */
	public static class ModelConverterOptions extends ConverterOptions {

		public ModelConverterOptions(org.jenerateit.modelconverter.ModelConverterOptions options) {
			super(options);
		}
		
		public boolean isInterfaceAdded() {
			Serializable addInterface = getOptions().get("add.interface");
			return addInterface == null || (new Boolean(addInterface.toString()).booleanValue() == true);
		}
	}
	
	/**
	 * @author mmt
	 *
	 * @param <S>
	 * @param <T>
	 */
	public static class ComplexTypeToClassConverter<S extends ComplexType, T extends JavaClass> extends AbstractM2MModelElementConverter<S, T> {

		public ComplexTypeToClassConverter(AbstractConverter modelConverter) {
			super(modelConverter, ModelElementConverterBehavior.DEFAULT);
		}
		
		@Override
		protected ModelConverterOptions getConverterOptions() {
			return ((ModelConverterPattern03)getModelConverter()).getConverterOptions();		}



		/* (non-Javadoc)
		 * @see com.gs.gapp.metamodel.converter.AbstractModelElementConverter#onCreateModelElement(java.lang.Object, com.gs.gapp.metamodel.basic.ModelElementI)
		 */
		@Override
		protected T onCreateModelElement(ComplexType type, ModelElementI previousResultingModelElement) {
			JavaPackage javaPackage = new JavaPackage("pattern03", null);
			@SuppressWarnings("unchecked")
			T result = (T) new JavaClass(type.getName(), javaPackage);
			return result;
		}

		/* (non-Javadoc)
		 * @see com.gs.gapp.metamodel.converter.AbstractM2MModelElementConverter#onConvert(com.gs.gapp.metamodel.basic.ModelElementI, com.gs.gapp.metamodel.basic.ModelElementI)
		 */
		@Override
		protected void onConvert(S type, T javaClass) {
			super.onConvert(type, javaClass);
			
			if (getConverterOptions().isInterfaceAdded()) {
			    // --- this call lets the conversion framework identify an execute an appropriate element converter 
			    convertWithOtherConverter(JavaInterface.class, javaClass);
			}
		}
	}
	
	/**
	 * @author mmt
	 *
	 * @param <S>
	 * @param <T>
	 */
	public static class ClassToInterfaceConverter<S extends JavaClass, T extends JavaInterface> extends AbstractM2MModelElementConverter<S, T> {

		public ClassToInterfaceConverter(AbstractConverter modelConverter) {
			super(modelConverter, ModelElementConverterBehavior.INDIRECT_CONVERSION_ONLY);
		}

		@Override
		protected T onCreateModelElement(JavaClass javaClass, ModelElementI previousResultingModelElement) {
			@SuppressWarnings("unchecked")
			T result = (T) new JavaInterface(javaClass.getName() + "I", 0 | JavaModifier.PUBLIC, javaClass.getJavaPackage());
			javaClass.addParentInterface(result);
			return result;
		}
	}
}
